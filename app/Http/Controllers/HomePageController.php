<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Configs;
use App\Models\Alumni;
use App\Models\Galleries;
use App\Models\Blog;
use App\Models\DosenProdi;
use App\Models\Sambutan;
use App\Models\BlogCategories;
use App\Models\GalleryCategories;
use App\Models\Archives;
use App\Models\TahunAkademik;
use App\Models\StrukturOrganisasi;
use App\Models\PekerjaanAlumni;

class HomePageController extends Controller
{
    public function dashboard()
    {
        $bidangit       = PekerjaanAlumni::where('name', 'Bidang IT')->first()->alumni_count;
        $PNS            = PekerjaanAlumni::where('name', 'PNS')->first()->alumni_cout;
        $wirausahawan   = PekerjaanAlumni::where('name', 'Wirausahawan')->first()->alumni_count;
        $olahraga       = PekerjaanAlumni::where('name', 'Olahraga')->first()->alumni_count;
        $dll            = PekerjaanAlumni::where('name', 'Dan Lain Lain')->first()->alumni_count;

        $views = [
            'BI'    => $bidangit,
            'PNS'   => $PNS,
            'W'     => $wirausahawan,
            'O'     => $olahraga,
            'DLL'   => $dll
        ];

        return view('admin.index')->with($views);
    }

    public function index(Request $request) 
    {
        $kurikulum = Configs::where('name', 'Kurikulum')->first()->value;
        $mahasiswa_dan_alumni = Configs::where('name', 'Mahasiswa dan alumni')->first()->value;
        $kerja_sama = Configs::where('name', 'Kerja Sama')->first()->value;
        $galleries = Galleries::with('category')->orderBy('created_at', 'DESC')->take(6)->get();
        $alumnis = Alumni::where('homepage', true)->where('verif', true)->get();
        $lastBlogs = Blog::orderBy('created_at', 'DESC')->take(3)->get();
        $homepageBlogs = Blog::orderBy('created_at', 'DESC')->where('homepage', true)->get();
        $jobs = PekerjaanAlumni::orderBy('id', 'ASC')->get();

        $data = [
            'kurikulum'                => $kurikulum,
            'mahasiswa_dan_alumni'     => $mahasiswa_dan_alumni,
            'kerja_sama'               => $kerja_sama,
            'alumnis'                  => $alumnis,
            'galleries'                => $galleries,
            'lastBlogs'                => $lastBlogs,
            'homepageBlogs'            => $homepageBlogs,
            'jobs'                     => $jobs
        ];

        return view('index')->with($data);
    }

    public function berita (Request $request)
    {
        $blogs = Blog::filter($request)->with('category', 'user')->orderBy('created_at', 'DESC')->paginate(6);
        $categories = BlogCategories::orderBy('name', 'ASC')->get();

        $data = [
            'blogs' => $blogs,
            'categories' => $categories
        ];

        return view('page.berita')->with($data);
    }

    public function beritaDetail($id)
    {
        $blog = Blog::with('user', 'category')->where('id', $id)->first();
        $categories = BlogCategories::orderBy('name', 'ASC')->get();

        $data = [
            'blog' => $blog,
            'categories' => $categories
        ];

        return view('page.berita_detail')->with($data);
    }

    public function profile ()
    {
        $sambutan = Sambutan::first();
        $visi     = Configs::where('name', 'Visi')->first()->value;
        $misi     = Configs::where('name', 'Misi')->first()->value;
        $tujuan   = Configs::where('name', 'Tujuan')->first()->value;
        $tujuan_pendidikan   = Configs::where('name', 'Tujuan Pendidikan')->first()->value;
        $profil_pendidikan   = Configs::where('name', 'Profil Pendidikan')->first()->value;

        $dosens   = DosenProdi::get();

        $data = [
            'sambutan' => $sambutan,
            'visi'     => $visi,
            'misi'     => $misi,
            'tujuan'   => $tujuan,
            'dosens'   => $dosens,
            'tujuan_pendidikan'   => $tujuan_pendidikan,
            'profil_pendidikan'   => $profil_pendidikan
        ];

        return view('page.profil')->with($data);
    }

    public function galleries (Request $request)
    {
        $galleries = Galleries::filter($request)->with('category')->orderBy('created_at', 'DESC')->paginate(12);
        $categories = GalleryCategories::orderBy('name', 'ASC')->get();

        $data = [
            'categories'         => $categories,
            'galleries'          => $galleries,
        ];
        return view('page.galleries')->with($data);
    }

    public function archives (Request $request)
    {
        $thn_akademik       = TahunAkademik::first()->tahun;
        $kalender_akademik  = Archives::where('type', 'kalender_akademik')->where('tahun', $thn_akademik)->get();
        $kurikulum          = Archives::where('type', 'kurikulum')->where('tahun', $thn_akademik)->get();
        $jadwal_perkuliahan = Archives::where('type', 'jadwal_perkuliahan')->where('tahun', $thn_akademik)->get();

        $data = [
            'TA'     => $thn_akademik,
            'KAs'    => $kalender_akademik,
            'Ks'     => $kurikulum,
            'JPs'    => $jadwal_perkuliahan,
        ];
        
        return view('page.archives')->with($data);
    }

    public function dosen (Request $request)
    {
        $dosens   = DosenProdi::filter($request)->orderBy('name', 'ASC')->paginate(10);
        $data = [
            'dosens'   => $dosens
        ];

        return view('page.dosen')->with($data);
    }

    public function StrukturOrganisasi (Request $request)
    {
        $struktur_organisasi   = StrukturOrganisasi::orderBy('created_at', 'DESC')->get();

        $data = [
            'struktur_organisasis' => $struktur_organisasi,
        ];

        return view('page.struktur_organisasi')->with($data);
    }

    public function kegiatan (Request $request)
    {
        $galleries = Galleries::filter($request)->with('category')->whereHas('category', function($q) {
           $q->where('name', 'LIKE', '%' . 'kemahasiswaan' . '%'); 
        })->orderBy('created_at', 'DESC')->paginate(12);

        $data = [
            'galleries'          => $galleries,
        ];
        return view('page.kegiatan')->with($data);
    }

    public function alumni (Request $request)
    {
        $alumni  = Alumni::filter($request)->orderBy('created_at', 'DESC')->paginate(10);

        $data = [
            'alumnis'     => $alumni,
        ];
        
        return view('page.alumni')->with($data);
    }
}
