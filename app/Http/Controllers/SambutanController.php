<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\Models\Sambutan;
use Illuminate\Support\Str;

class SambutanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sambutan = Sambutan::orderBy('created_at', 'DESC')->first();
        $view = [
            'data'  => $sambutan
        ];

        return view('admin.sambutan.list')->with($view);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $sambutan = Sambutan::findOrFail($id);
            $sambutan->update([
                'name' => $request->nama,
                'quote' => $request->quote,
            ]);
    
            if (isset($request->img)) {
                if (!empty($sambutan->img)) {
                    \File::delete(public_path('storage/sambutan/' . $sambutan->img));
                }
    
                $file = $request->img;
                $slug = Str::slug($file->getClientOriginalName(), '_');
                $photo = time() . $slug . '.' . $file->getClientOriginalExtension();
                $sambutan->update(['img' => $photo]);
    
    
                $this->SavePhotoNR($file, $photo, public_path('storage/sambutan/'));
            }

            DB::commit();
            
            $request->session()->flash('messages', 'Berhasil Mengubah Data Sambutan');
            $request->session()->flash('type', 'success');
    
            return redirect()->back();
        } catch (\Exception $e) {
            DB::rollback();
        
            return $e->getMessage();
        }
    }

    private function SavePhotoNR($image, $photo, $path)
    {
        $file = $image;
        $file->move($path, $photo);
    }
}
