<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\DosenProdi;

class DosenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $dosen = DosenProdi::filter($request)->orderBy('name', 'ASC')->paginate(10);
        $view = [
            'datas'  => $dosen
        ];

        return view('admin.dosen.list')->with($view);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.dosen.tambah');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            
            $file = $request->img;
            $slug = Str::slug($file->getClientOriginalName(), '_');
            $photo = time() . $slug . '.' . $file->getClientOriginalExtension();
            
            DosenProdi::create([
                'name'              => $request->name,
                'kejuruan'          => $request->kejuruan,
                'telp'              => $request->no_telepon,
                'img'               => $photo
            ]);
            
            $this->SavePhotoNR($file, $photo, public_path('storage/dosens/'));

            DB::commit();
            
            $request->session()->flash('messages', 'Berhasil Menambahkan Data Dosen');
            $request->session()->flash('type', 'success');
    
            return redirect()->route('dosen.index');
        } catch (\Exception $e) {
            DB::rollback();
        
            return $e->getMessage();
        }
    }

    private function SavePhotoNR($image, $photo, $path)
    {
        $file = $image;
        $file->move($path, $photo);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = DosenProdi::findOrFail($id);
        $view = [
            'data'       => $data
        ];

        return view('admin.dosen.tambah')->with($view);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $dosen = DosenProdi::findOrFail($id);
            $dosen->update([
                'name'              => $request->name,
                'kejuruan'          => $request->kejuruan,
                'telp'              => $request->no_telepon,
            ]);
    
            if (isset($request->img)) {
                if (!empty($dosen->img)) {
                    \File::delete(public_path('storage/dosens/' . $dosen->img));
                }
    
                $file = $request->img;
                $slug = Str::slug($file->getClientOriginalName(), '_');
                $photo = time() . $slug . '.' . $file->getClientOriginalExtension();
                $dosen->update(['img' => $photo]);
    
                $this->SavePhotoNR($file, $photo, public_path('storage/dosens/'));
            }

            DB::commit();
            
            $request->session()->flash('messages', 'Berhasil Mengubah Data dosen');
            $request->session()->flash('type', 'success');
    
            return redirect()->route('dosen.index');
        } catch (\Exception $e) {
            DB::rollback();
        
            return $e->getMessage();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $dosen = DosenProdi::findOrFail($id);
            if (!empty($dosen->img)) {
                \File::delete(public_path('storage/dosens/' . $dosen->img));
            }
            
            $dosen->delete();

            DB::commit();

            $request->session()->flash('messages', 'Berhasil Menghapus Data dosen');
            $request->session()->flash('type', 'success');
    
            return redirect()->back();
        } catch (\Exception $e) {
            DB::rollback();
        
            return $e->getMessage();
        }
    }
}
