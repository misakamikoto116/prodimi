<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\GalleryCategories;
use DB;

class CategoryGalleryController extends Controller
{
   /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $gallery_category = GalleryCategories::filter($request)->with('galleries')->orderBy('name', 'ASC')->paginate(10);
        $view = [
            'datas'  => $gallery_category
        ];

        return view('admin.gallery_category.list')->with($view);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            GalleryCategories::create([
                'name' => $request->name
            ]);

            DB::commit();

            $request->session()->flash('messages', 'Berhasil Membuat Data Kategori gallery');
            $request->session()->flash('type', 'success');
    
            return redirect()->back();
        } catch (\Exception $e) {
            DB::rollback();
        
            return $e->getMessage();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $config = GalleryCategories::findOrFail($id);
            $config->update([
                'name' => $request->name
            ]);
        
            DB::commit();
            $request->session()->flash('messages', 'Berhasil Mengubah Data Kategori gallery');
            $request->session()->flash('type', 'success');

            return redirect()->back();
        } catch (\Exception $e) {
            DB::rollback();
        
            return $e->getMessage();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $gallery_category = GalleryCategories::findOrFail($id);
            if ($gallery_category->galleries->count() > 0) {
                $request->session()->flash('messages', 'Tidak dapat dihapus karena sudah dipakai di gallery');
                $request->session()->flash('type', 'danger');
        
                return redirect()->back();
            }

            $gallery_category->delete();

            DB::commit();

            $request->session()->flash('messages', 'Berhasil Menghapus Data Kategori gallery');
            $request->session()->flash('type', 'success');
    
            return redirect()->back();
        } catch (\Exception $e) {
            DB::rollback();
        
            return $e->getMessage();
        }
    }
}
