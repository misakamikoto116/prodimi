<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\GalleryCategories;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
   /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function profile(Request $request)
    {
        return view('admin.profiles.profile');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function profilePost(Request $request)
    {
        DB::beginTransaction();
        try {
            if (! Hash::check($request->password, auth()->user()->password)) {
                $request->session()->flash('messages', 'Password Anda Salah');
                $request->session()->flash('type', 'danger');
                return redirect()->back();
            }

            auth()->user()->update([
                'name'  => $request->name,
            ]);

            if (isset($request->img)) {
                if (!empty(auth()->user()->img)) {
                    \File::delete(public_path('storage/users/' . auth()->user()->img));
                }
    
                $file = $request->img;
                $slug = Str::slug($file->getClientOriginalName(), '_');
                $photo = time() . $slug . '.' . $file->getClientOriginalExtension();
                auth()->user()->update(['img' => $photo]);
    
                $this->SavePhotoNR($file, $photo, public_path('storage/users/'));
            }

            DB::commit();

            $request->session()->flash('messages', 'Berhasil Mengubah Profile');
            $request->session()->flash('type', 'success');
    
            return redirect()->back();
        } catch (\Exception $e) {
            DB::rollback();
        
            return $e->getMessage();
        }
    }

    private function SavePhotoNR($image, $photo, $path)
    {
        $file = $image;
        $file->move($path, $photo);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function password(Request $request)
    {
        return view('admin.profiles.password');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function passwordPost(Request $request)
    {
        DB::beginTransaction();
        try {
            if (! Hash::check($request->password, auth()->user()->password)) {
                $request->session()->flash('messages', 'Password Anda Salah');
                $request->session()->flash('type', 'danger');
                return redirect()->back();
            }

            auth()->user()->update([
                'password'  => bcrypt($request->new_password),
            ]);

            DB::commit();

            $request->session()->flash('messages', 'Berhasil Mengubah Password');
            $request->session()->flash('type', 'success');
    
            return redirect()->back();
        } catch (\Exception $e) {
            DB::rollback();
        
            return $e->getMessage();
        }
    }
}
