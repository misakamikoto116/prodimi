<?php

namespace App\Http\Controllers;

use DB;
use App\User;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = User::filter($request)->with('blogs')->whereNotIn('id', [1])->orderBy('name', 'ASC')->paginate(10);
        $view = [
            'datas'  => $user
        ];

        return view('admin.user.list')->with($view);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.user.tambah');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        DB::beginTransaction();
        try {
            
            $file = $request->img;
            $slug = Str::slug($file->getClientOriginalName(), '_');
            $photo = time() . $slug . '.' . $file->getClientOriginalExtension();
            
            User::create([
                'name'              => $request->name,
                'email'             => $request->email,
                'password'          => bcrypt($request->password),
                'img'               => $photo
            ]);
            
            $this->SavePhotoNR($file, $photo, public_path('storage/users/'));

            DB::commit();
            
            $request->session()->flash('messages', 'Berhasil Menambahkan Data User');
            $request->session()->flash('type', 'success');
    
            return redirect()->route('user.index');
        } catch (\Exception $e) {
            DB::rollback();
        
            return $e->getMessage();
        }
    }

    private function SavePhotoNR($image, $photo, $path)
    {
        $file = $image;
        $file->move($path, $photo);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = User::findOrFail($id);
        $view = [
            'data'       => $data,
        ];

        return view('admin.user.tambah')->with($view);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, $id)
    {
        DB::beginTransaction();
        try {
            $user = User::findOrFail($id);
            $user->update([
                'name'        => $request->name,
            ]);
    
            if (isset($request->img)) {
                if (!empty($user->img)) {
                    \File::delete(public_path('storage/users/' . $user->img));
                }
    
                $file = $request->img;
                $slug = Str::slug($file->getClientOriginalName(), '_');
                $photo = time() . $slug . '.' . $file->getClientOriginalExtension();
                $user->update(['img' => $photo]);
    
                $this->SavePhotoNR($file, $photo, public_path('storage/users/'));
            }

            DB::commit();
            
            $request->session()->flash('messages', 'Berhasil Mengubah Data user');
            $request->session()->flash('type', 'success');
    
            return redirect()->route('user.index');
        } catch (\Exception $e) {
            DB::rollback();
        
            return $e->getMessage();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $user = User::findOrFail($id);
            if (!empty($user->img)) {
                \File::delete(public_path('storage/users/' . $user->img));
            }
            
            $user->delete();

            DB::commit();

            $request->session()->flash('messages', 'Berhasil Menghapus Data User');
            $request->session()->flash('type', 'success');
    
            return redirect()->back();
        } catch (\Exception $e) {
            DB::rollback();
        
            return $e->getMessage();
        }
    }
}
