<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\BlogCategories;
use DB;

class CategoryBlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $blog_category = BlogCategories::filter($request)->with('blogs')->orderBy('name', 'ASC')->paginate(10);
        $view = [
            'datas'  => $blog_category
        ];

        return view('admin.blog_category.list')->with($view);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            BlogCategories::create([
                'name' => $request->name
            ]);

            DB::commit();

            $request->session()->flash('messages', 'Berhasil Membuat Data Kategori Blog');
            $request->session()->flash('type', 'success');
    
            return redirect()->back();
        } catch (\Exception $e) {
            DB::rollback();
        
            return $e->getMessage();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $config = BlogCategories::findOrFail($id);
            $config->update([
                'name' => $request->name
            ]);
        
            DB::commit();
            $request->session()->flash('messages', 'Berhasil Mengubah Data Kategori Blog');
            $request->session()->flash('type', 'success');

            return redirect()->back();
        } catch (\Exception $e) {
            DB::rollback();
        
            return $e->getMessage();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $blog_category = BlogCategories::findOrFail($id);
            if ($blog_category->blogs->count() > 0) {
                $request->session()->flash('messages', 'Tidak dapat dihapus karena sudah dipakai di blog');
                $request->session()->flash('type', 'danger');
        
                return redirect()->back();
            }

            $blog_category->delete();

            DB::commit();

            $request->session()->flash('messages', 'Berhasil Menghapus Data Kategori Blog');
            $request->session()->flash('type', 'success');
    
            return redirect()->back();
        } catch (\Exception $e) {
            DB::rollback();
        
            return $e->getMessage();
        }
    }
}
