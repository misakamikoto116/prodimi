<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\Models\BerkasKemahasiswaan;
use Illuminate\Support\Str;

class BerkasKemahasiswaanController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $berkas = BerkasKemahasiswaan::Filter($request)->orderBy('tahun', 'DESC')->paginate(10);

        $view = [
            'datas'  => $berkas
        ];

        return view('admin.archive.berkas_kemahasiswaan.list')->with($view);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {

            $file = $request->img;
            $slug = Str::slug($file->getClientOriginalName(), '_');
            $photo = time() . $slug . '.' . $file->getClientOriginalExtension();

            $this->SavePhotoNR($file, $photo, public_path('storage/berkas-kemahasiswaan/'));

            BerkasKemahasiswaan::create([
                'name'  => $request->name,
                'tahun' => $request->tahun,
                'file' => $photo,
            ]);
    

            DB::commit();
            
            $request->session()->flash('messages', 'Berhasil Menambahkan Berkas Kemahasiswaan');
            $request->session()->flash('type', 'success');
    
            return redirect()->back();
        } catch (\Exception $e) {
            DB::rollback();
        
            return $e->getMessage();
        }
    }

    private function SavePhotoNR($image, $photo, $path)
    {
        $file = $image;
        $file->move($path, $photo);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $berkas = BerkasKemahasiswaan::findOrFail($id);
            $berkas->update([
                'name' => $request->name,
                'tahun' => $request->tahun,
            ]);
    
            if (isset($request->img)) {
                if (!empty($berkas->file)) {
                    \File::delete(public_path('storage/berkas-kemahasiswaan/' . $berkas->file));
                }
    
                $file = $request->img;
                $slug = Str::slug($file->getClientOriginalName(), '_');
                $photo = time() . $slug . '.' . $file->getClientOriginalExtension();
                $berkas->update(['file' => $photo]);
    
    
                $this->SavePhotoNR($file, $photo, public_path('storage/berkas-kemahasiswaan/'));
            }

            DB::commit();
            
            $request->session()->flash('messages', 'Berhasil Mengubah Data Berkas Kemahasiswaan');
            $request->session()->flash('type', 'success');
    
            return redirect()->back();
        } catch (\Exception $e) {
            DB::rollback();
        
            return $e->getMessage();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $berkas = BerkasKemahasiswaan::findOrFail($id);
            if (!empty($berkas->file)) {
                \File::delete(public_path('storage/berkas-kemahasiswaan/' . $berkas->file));
            }
            
            $berkas->delete();

            DB::commit();

            $request->session()->flash('messages', 'Berhasil Menghapus Data Berkas Kemahasiswaan');
            $request->session()->flash('type', 'success');
    
            return redirect()->back();
        } catch (\Exception $e) {
            DB::rollback();
        
            return $e->getMessage();
        }
    }
}
