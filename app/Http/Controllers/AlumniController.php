<?php

namespace App\Http\Controllers;

use DB;
use Excel;
use App\Models\Alumni;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Exports\AlumnisExport;
use App\Models\PekerjaanAlumni;

class AlumniController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $alumni = Alumni::filter($request)->with('job')->orderBy('nama', 'ASC')->paginate(10);
        $view = [
            'datas'  => $alumni
        ];

        return view('admin.alumni.list')->with($view);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $jobs = PekerjaanAlumni::orderBy('id', 'ASC')->get();
        $views = [
            'jobs' => $jobs
        ];

        return view('admin.alumni.tambah')->with($views);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {

            $routeName = $request->route()->getName();
            $module = explode('.', $routeName)[0];
            
            $file = $request->img;
            $slug = Str::slug($file->getClientOriginalName(), '_');
            $photo = time() . $slug . '.' . $file->getClientOriginalExtension();
            
            Alumni::create([
                'nama'              => $request->name,
                'angkatan'          => $request->tahun_angkatan,
                'job_id'            => $request->job_id,
                'telp'              => $request->no_telepon,
                'quote'             => $request->quote,
                'img'               => $photo
            ]);
            
            $this->SavePhotoNR($file, $photo, public_path('storage/alumnis/'));

            DB::commit();
            
            $request->session()->flash('messages', 'Berhasil Menambahkan Data Alumni');
            $request->session()->flash('type', 'success');
            
            if ($module === 'alumni') {
                $redirect = redirect()->route('alumni.index');
            } else {
                $redirect = redirect()->route('index');
            }

            return $redirect;
        } catch (\Exception $e) {
            DB::rollback();
        
            return $e->getMessage();
        }
    }

    private function SavePhotoNR($image, $photo, $path)
    {
        $file = $image;
        $file->move($path, $photo);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Alumni::findOrFail($id);
        $jobs = PekerjaanAlumni::orderBy('id', 'ASC')->get();

        $view = [
            'data'       => $data,
            'jobs'       => $jobs
        ];

        return view('admin.alumni.tambah')->with($view);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $alumni = Alumni::findOrFail($id);
            $alumni->update([
                'nama'              => $request->name,
                'angkatan'          => $request->tahun_angkatan,
                'job_id'            => $request->job_id,
                'telp'              => $request->no_telepon,
                'quote'             => $request->quote,
            ]);
    
            if (isset($request->img)) {
                if (!empty($alumni->img)) {
                    \File::delete(public_path('storage/alumnis/' . $alumni->img));
                }
    
                $file = $request->img;
                $slug = Str::slug($file->getClientOriginalName(), '_');
                $photo = time() . $slug . '.' . $file->getClientOriginalExtension();
                $alumni->update(['img' => $photo]);
    
                $this->SavePhotoNR($file, $photo, public_path('storage/alumnis/'));
            }

            DB::commit();
            
            $request->session()->flash('messages', 'Berhasil Mengubah Data Alumni');
            $request->session()->flash('type', 'success');
    
            return redirect()->route('alumni.index');
        } catch (\Exception $e) {
            DB::rollback();
        
            return $e->getMessage();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $alumni = Alumni::findOrFail($id);
            if (!empty($alumni->img)) {
                \File::delete(public_path('storage/alumnis/' . $alumni->img));
            }
            
            $alumni->delete();

            DB::commit();

            $request->session()->flash('messages', 'Berhasil Menghapus Data alumni');
            $request->session()->flash('type', 'success');
    
            return redirect()->back();
        } catch (\Exception $e) {
            DB::rollback();
        
            return $e->getMessage();
        }
    }

    public function homepageManage (Request $request, $id) {
        DB::beginTransaction();
        try {
            $blog = Alumni::findOrFail($id);
            $blog->update([
                'homepage' => !$blog->homepage
            ]);

            DB::commit();
            
            $request->session()->flash('messages', 'Berhasil Mengubah Status Alumni');
            $request->session()->flash('type', 'success');
    
            return redirect()->route('alumni.index');
        } catch (\Exception $e) {
            DB::rollback();
        
            return $e->getMessage();
        }
    }

    public function verification (Request $request, $id) {
        DB::beginTransaction();
        try {
            $blog = Alumni::findOrFail($id);
            $blog->update([
                'verif' => true
            ]);

            DB::commit();
            
            $request->session()->flash('messages', 'Berhasil Memverifikasi Alumni');
            $request->session()->flash('type', 'success');
    
            return redirect()->route('alumni.index');
        } catch (\Exception $e) {
            DB::rollback();
        
            return $e->getMessage();
        }
    }

    public function print() {
        return Excel::download(new AlumnisExport, 'alumnis.xlsx');
    }
}
