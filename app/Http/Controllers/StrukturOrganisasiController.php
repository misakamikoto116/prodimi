<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\Models\StrukturOrganisasi;

class StrukturOrganisasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $struktur_organisasi = StrukturOrganisasi::filter($request)->orderBy('created_at', 'DESC')->paginate(10);
        $view = [
            'datas'  => $struktur_organisasi
        ];

        return view('admin.struktur_organisasi.list')->with($view);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $struktur = StrukturOrganisasi::findOrFail($id);
            $struktur->update([
                'name'      => $request->name,
                'nim'       => $request->NIM
            ]);
        
            DB::commit();
            $request->session()->flash('messages', 'Berhasil Mengubah Data Struktur Organisasi');
            $request->session()->flash('type', 'success');

            return redirect()->back();
        } catch (\Exception $e) {
            DB::rollback();
        
            return $e->getMessage();
        }
    }
}
