<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TahunAkademik;
use DB;

class TahunAkademikController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $config = TahunAkademik::orderBy('created_at', 'DESC')->first();
        $view = [
            'data'  => $config
        ];

        return view('admin.tahun_akademik.list')->with($view);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $config = TahunAkademik::findOrFail($id);
            $config->update([
                'tahun' => $request->tahun
            ]);
        
            DB::commit();
            $request->session()->flash('messages', 'Berhasil Mengubah Tahun Akademik');
            $request->session()->flash('type', 'success');

            return redirect()->back();
        } catch (\Exception $e) {
            DB::rollback();
        
            return $e->getMessage();
        }
    }
}
