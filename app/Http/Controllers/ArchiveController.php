<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\Models\Archives;
use Illuminate\Support\Str;

class ArchiveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $archives = Archives::Filter($request)->orderBy('tahun', 'DESC')->paginate(10);
        $routeName = $request->route()->getName();
        $module = explode('.', $routeName)[0];

        $view = [
            'datas'  => $archives
        ];

        switch ($module) {
            case 'jadwal-perkuliahan':
                $rturn = view('admin.archive.jadwal_perkuliahan.list')->with($view);
                break;
                
            case 'kalender-akademik':
                $rturn = view('admin.archive.kalender_akademik.list')->with($view);
            break;
            
            default:
                $rturn =  view('admin.archive.kurikulum.list')->with($view);
                break;
        }

        return $rturn;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $routeName = $request->route()->getName();
            $module = explode('.', $routeName)[0];

            switch ($module) {
                case 'jadwal-perkuliahan':
                    $type = 'jadwal_perkuliahan';
                    break;

                case 'kalender-akademik':
                    $type = 'kalender_akademik';
                    break;
                
                default:
                    $type = 'kurikulum';
                    break;
            }

            $file = $request->img;
            $slug = Str::slug($file->getClientOriginalName(), '_');
            $photo = time() . $slug . '.' . $file->getClientOriginalExtension();

            $this->SavePhotoNR($file, $photo, public_path('storage/' . $module . '/'));

            Archives::create([
                'tahun' => $request->tahun,
                'file' => $photo,
                'type' => $type
            ]);
    

            DB::commit();
            
            $request->session()->flash('messages', 'Berhasil Menambahkan archive');
            $request->session()->flash('type', 'success');
    
            return redirect()->back();
        } catch (\Exception $e) {
            DB::rollback();
        
            return $e->getMessage();
        }
    }

    private function SavePhotoNR($image, $photo, $path)
    {
        $file = $image;
        $file->move($path, $photo);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $routeName = $request->route()->getName();
            $module = explode('.', $routeName)[0];

            $archive = Archives::findOrFail($id);
            $archive->update([
                'tahun' => $request->tahun,
            ]);
    
            if (isset($request->img)) {
                if (!empty($archive->file)) {
                    \File::delete(public_path('storage/' . $module .  '/' . $archive->file));
                }
    
                $file = $request->img;
                $slug = Str::slug($file->getClientOriginalName(), '_');
                $photo = time() . $slug . '.' . $file->getClientOriginalExtension();
                $archive->update(['file' => $photo]);
    
    
                $this->SavePhotoNR($file, $photo, public_path('storage/' . $module .'/'));
            }

            DB::commit();
            
            $request->session()->flash('messages', 'Berhasil Mengubah Data Archive');
            $request->session()->flash('type', 'success');
    
            return redirect()->back();
        } catch (\Exception $e) {
            DB::rollback();
        
            return $e->getMessage();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $routeName = $request->route()->getName();
            $module = explode('.', $routeName)[0];
            
            $archive = Archives::findOrFail($id);
            if (!empty($archive->file)) {
                \File::delete(public_path('storage/' . $module .  '/' . $archive->file));
            }
            
            $archive->delete();


            DB::commit();

            $request->session()->flash('messages', 'Berhasil Menghapus Data Archive');
            $request->session()->flash('type', 'success');
    
            return redirect()->back();
        } catch (\Exception $e) {
            DB::rollback();
        
            return $e->getMessage();
        }
    }
}
