<?php

namespace App\Http\Controllers;

use DB;
use App\Models\Blog;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\BlogCategories;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $blog = Blog::Filter($request)->with('user', 'category')->orderBy('created_at', 'DESC')->paginate(10);
        $view = [
            'datas'  => $blog
        ];

        return view('admin.blog.list')->with($view);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = BlogCategories::get();
        $view = [
            'categories' => $category
        ];

        return view('admin.blog.tambah')->with($view);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            
            $file = $request->img;
            $slug = Str::slug($file->getClientOriginalName(), '_');
            $photo = time() . $slug . '.' . $file->getClientOriginalExtension();
            
            Blog::create([
                'title'             => $request->title,
                'category_id'       => $request->category,
                'thumbnail'         => $photo,
                'user_id'           => auth()->user()->id,
                'description'       => $request->desc
            ]);
            
            $this->SavePhotoNR($file, $photo, public_path('storage/blogs/'));

            DB::commit();
            
            $request->session()->flash('messages', 'Berhasil Menambahkan Data Blog');
            $request->session()->flash('type', 'success');
    
            return redirect()->route('blog.index');
        } catch (\Exception $e) {
            DB::rollback();
        
            return $e->getMessage();
        }
    }

    private function SavePhotoNR($image, $photo, $path)
    {
        $file = $image;
        $file->move($path, $photo);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = BlogCategories::get();
        $data = Blog::findOrFail($id);
        $view = [
            'categories' => $category,
            'data'       => $data
        ];

        return view('admin.blog.tambah')->with($view);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $blog = Blog::findOrFail($id);
            $blog->update([
                'title'             => $request->title,
                'category_id'       => $request->category,
                'description'       => $request->desc
            ]);
    
            if (isset($request->img)) {
                if (!empty($blog->thumbnail)) {
                    \File::delete(public_path('storage/blogs/' . $blog->thumbnail));
                }
    
                $file = $request->img;
                $slug = Str::slug($file->getClientOriginalName(), '_');
                $photo = time() . $slug . '.' . $file->getClientOriginalExtension();
                $blog->update(['thumbnail' => $photo]);
    
                $this->SavePhotoNR($file, $photo, public_path('storage/blogs/'));
            }

            DB::commit();
            
            $request->session()->flash('messages', 'Berhasil Mengubah Data Blog');
            $request->session()->flash('type', 'success');
    
            return redirect()->route('blog.index');
        } catch (\Exception $e) {
            DB::rollback();
        
            return $e->getMessage();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $blog = Blog::findOrFail($id);
            if (!empty($blog->thumbnail)) {
                \File::delete(public_path('storage/blogs/' . $blog->thumbnail));
            }
            
            $blog->delete();

            DB::commit();

            $request->session()->flash('messages', 'Berhasil Menghapus Data Blog');
            $request->session()->flash('type', 'success');
    
            return redirect()->back();
        } catch (\Exception $e) {
            DB::rollback();
        
            return $e->getMessage();
        }
    }

    public function homepageManage (Request $request, $id) {
        DB::beginTransaction();
        try {
            $blog = Blog::findOrFail($id);
            $blog->update([
                'homepage' => !$blog->homepage
            ]);

            DB::commit();
            
            $request->session()->flash('messages', 'Berhasil Mengubah Status Homepage Blog');
            $request->session()->flash('type', 'success');
    
            return redirect()->route('blog.index');
        } catch (\Exception $e) {
            DB::rollback();
        
            return $e->getMessage();
        }
    }
}
