<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\Models\Configs;


class ConfigController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $config = Configs::orderBy('name', 'ASC')->get();
        $view = [
            'datas'  => $config
        ];

        return view('admin.config.list')->with($view);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $config = Configs::findOrFail($id);
            $config->update([
                'value' => $request->value
            ]);
        
            DB::commit();
            $request->session()->flash('messages', 'Berhasil Mengubah Data Config');
            $request->session()->flash('type', 'success');

            return redirect()->back();
        } catch (\Exception $e) {
            DB::rollback();
        
            return $e->getMessage();
        }
    }
}
