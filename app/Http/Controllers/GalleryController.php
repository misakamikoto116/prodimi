<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Galleries;
use DB;
use App\Models\GalleryCategories;
use Illuminate\Support\Str;

class GalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $gallery_category = Galleries::filter($request)->with('category')->orderBy('created_at', 'DESC')->paginate(10);
        $view = [
            'datas'  => $gallery_category
        ];

        return view('admin.gallery.list')->with($view);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = GalleryCategories::get();
        $view = [
            'categories' => $category
        ];

        return view('admin.gallery.tambah')->with($view);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            
            $file = $request->img;
            $slug = Str::slug($file->getClientOriginalName(), '_');
            $photo = time() . $slug . '.' . $file->getClientOriginalExtension();
            
            Galleries::create([
                'name' => $request->nama,
                'gallery_category_id' => $request->category,
                'img'               => $photo
            ]);
            
            $this->SavePhotoNR($file, $photo, public_path('storage/galleries/'));

            DB::commit();
            
            $request->session()->flash('messages', 'Berhasil Mengubah Data Galeri');
            $request->session()->flash('type', 'success');
    
            return redirect()->route('gallery.index');
        } catch (\Exception $e) {
            DB::rollback();
        
            return $e->getMessage();
        }
    }

    private function SavePhotoNR($image, $photo, $path)
    {
        $file = $image;
        $file->move($path, $photo);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = GalleryCategories::get();
        $data = Galleries::findOrFail($id);
        $view = [
            'categories' => $category,
            'data'       => $data
        ];

        return view('admin.gallery.tambah')->with($view);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $gallery = Galleries::findOrFail($id);
            $gallery->update([
                'name' => $request->nama,
                'category' => $request->category,
            ]);
    
            if (isset($request->img)) {
                if (!empty($gallery->img)) {
                    \File::delete(public_path('storage/galleries/' . $gallery->img));
                }
    
                $file = $request->img;
                $slug = Str::slug($file->getClientOriginalName(), '_');
                $photo = time() . $slug . '.' . $file->getClientOriginalExtension();
                $gallery->update(['img' => $photo]);
    
                $this->SavePhotoNR($file, $photo, public_path('storage/galleries/'));
            }

            DB::commit();
            
            $request->session()->flash('messages', 'Berhasil Mengubah Data Galeri');
            $request->session()->flash('type', 'success');
    
            return redirect()->route('gallery.index');
        } catch (\Exception $e) {
            DB::rollback();
        
            return $e->getMessage();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $gallery = Galleries::findOrFail($id);
            if (!empty($gallery->file)) {
                \File::delete(public_path('storage/galleries/' . $gallery->img));
            }
            
            $gallery->delete();

            DB::commit();

            $request->session()->flash('messages', 'Berhasil Menghapus Data Galeri');
            $request->session()->flash('type', 'success');
    
            return redirect()->back();
        } catch (\Exception $e) {
            DB::rollback();
        
            return $e->getMessage();
        }
    }
}
