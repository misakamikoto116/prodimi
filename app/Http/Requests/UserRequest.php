<?php

namespace App\Http\Requests;

use App\User;
use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->isMethod('PUT') || $this->isMethod('PATCH')) {
            $email = 'nullable';
            $pass = 'nullable';
        } else {
            $email = 'required|unique:users,email';
            $pass = 'required|min:6|max:20';
        }

        return [
            'name'=>'required|max:120',
            'email' => $email,
            'password' => $pass,
        ];
    }
}
