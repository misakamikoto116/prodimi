<?php

namespace App\Exports;

use App\Models\Alumni;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class AlumnisExport implements FromView
{
    public function view(): View
    {
        $data = Alumni::orderBy('nama', 'ASC')->where('verif', true)->with('job')->get();

        return view('exports.alumni', [
            'datas' => $data
        ]);
    }
}
