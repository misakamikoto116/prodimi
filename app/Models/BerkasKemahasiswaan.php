<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BerkasKemahasiswaan extends Model
{
    protected $table = 'berkas_kemahasiswaan';

    protected $fillable = [
        'name',
        'tahun',
        'file',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    public $timestamps = true;

    public function scopeFilter($query, $request)
    {
        if ($request->has('q')) {
            $query->where('name', 'like', '%' . $request->get('q') . '%')
                  ->orWhere('tahun', 'like', '%' . $request->get('q') . '%');
        }

        return $query;
    }
}
