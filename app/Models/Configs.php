<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Configs extends Model
{
    protected $fillable = [
        'id',
        'name',
        'value',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    public $timestamps = true;
}
