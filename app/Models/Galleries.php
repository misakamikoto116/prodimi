<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Galleries extends Model
{
    protected $fillable = [
        'id',
        'name',
        'img',
        'gallery_category_id'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    public $timestamps = true;

    public function category()
    {
        return $this->belongsTo(GalleryCategories::class, 'gallery_category_id', 'id');
    }

    public function scopeFilter($query, $request)
    {
        if ($request->has('q')) {
            $query->where('name', 'like', '%' . $request->get('q') . '%')
                   ->orWhereHas('category', function($q) use($request) {
                    $q->where('name', 'like', '%' . $request->get('q') . '%');
                  });
        }

        if ($request->has('category')) {
            $query->where('gallery_category_id', $request->get('category'));
        }

        return $query;
    }
}
