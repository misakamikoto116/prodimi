<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PekerjaanAlumni extends Model
{
    protected $appends = ['alumni_count'];
    public function alumni()
    {
        return $this->hasMany(Alumni::class, 'job_id');
    }

    public function getAlumniCountAttribute()
    {
        return $this->alumni->where('verif', true)->count();
    }
}
