<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Alumni extends Model
{
    protected $fillable = [
        'nama',
        'angkatan',
        'job_id',
        'telp',
        'quote',
        'verif',
        'homepage',
        'img',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $casts = [
        'homepage' => 'boolean',
        'verif'    => 'boolean'
    ];

    public $timestamps = true;
    
    public function job()
    {
        return $this->belongsTo(PekerjaanAlumni::class, 'job_id');
    }

    public function scopeFilter($query, $request)
    {
        if ($request->has('q')) {
            $query->where('nama', 'like', '%' . $request->get('q') . '%')
                   ->orWhere('angkatan', 'like', '%' . $request->get('q') . '%')
                   ->orWhereHas('job', function($q) use($request) {
                        $q->where('name', 'like', '%' . $request->get('q') . '%');
                    })
                   ->orWhere('telp', 'like', '%' . $request->get('q') . '%');
        }

        if ($request->has('homepage')) {
            $query->where('homepage', $request->get('homepage'));
        }

        if ($request->has('verif')) {
            $query->where('verif', $request->get('verif'));
        }

        return $query;
    }
}
