<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Archives extends Model
{
    protected $fillable = [
        'tahun',
        'file',
        'type',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    public $timestamps = true;

    public function scopeFilter($query, $request)
    {
        $routeName = $request->route()->getName();
        if ($request->has('q')) {
            $query->where('tahun', 'like', '%' . $request->get('q') . '%');
        }

        switch ($routeName) {
            case 'kurikulum.index':
                $query->where('type', 'kurikulum');
                break;

            case 'jadwal-perkuliahan.index':
                $query->where('type', 'jadwal_perkuliahan');
                break;

            case 'kalender-akademik.index':
                $query->where('type', 'kalender_akademik');
                break;
            
            default:
                break;
        }

        return $query;
    }
}
