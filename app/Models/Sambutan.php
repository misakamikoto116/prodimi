<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sambutan extends Model
{
    protected $fillable = [
        'id',
        'name',
        'quote',
        'img',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    public $timestamps = true;
}
