<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GalleryCategories extends Model
{
    protected $fillable = [
        'id',
        'name',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    public $timestamps = true;

    public function galleries()
    {
        return $this->hasMany(Galleries::class, 'gallery_category_id', 'id');
    }

    public function scopeFilter($query, $request)
    {
        if ($request->has('q')) {
            $query->where('name', 'like', '%' . $request->get('q') . '%');
        }

        return $query;
    }
}
