<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StrukturOrganisasi extends Model
{
    protected $fillable = [
        'name',
        'jabatan',
        'nim',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    public $timestamps = true;

    public function scopeFilter($query, $request)
    {
        if ($request->has('q')) {
            $query->where('name', 'like', '%' . $request->get('q') . '%')
                   ->orWhere('jabatan', 'like', '%' . $request->get('q') . '%')
                   ->orWhere('nim', 'like', '%' . $request->get('q') . '%');
        }

        return $query;
    }
}
