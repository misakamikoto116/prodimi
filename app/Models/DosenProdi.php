<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DosenProdi extends Model
{
    protected $fillable = [
        'name',
        'kejuruan',
        'telp',
        'img',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    public $timestamps = true;

    public function scopeFilter($query, $request)
    {
        if ($request->has('q')) {
            $query->where('name', 'like', '%' . $request->get('q') . '%')
                   ->orWhere('kejuruan', 'like', '%' . $request->get('q') . '%')
                   ->orWhere('telp', 'like', '%' . $request->get('q') . '%');
        }

        return $query;
    }
}
