<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Blog extends Model
{
    protected $fillable = [
        'id',
        'title',
        'user_id',
        'category_id',
        'description',
        'thumbnail',
        'homepage'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $casts = [
        'homepage' => 'boolean'
    ];

    public $timestamps = true;

    protected $appends = ['sort_desc'];

    public function category()
    {
        return $this->belongsTo(BlogCategories::class, 'category_id', 'id');
    }
    
    public function scopeFilter($query, $request)
    {
        if ($request->has('q')) {
            $query->where('title', 'like', '%' . $request->get('q') . '%')
                  ->orWhereHas('user', function($q) use($request) {
                    $q->where('name', 'like', '%' . $request->get('q') . '%');
                  })
                  ->orWhereHas('category', function($q) use($request) {
                    $q->where('name', 'like', '%' . $request->get('q') . '%');
                  });; 
        }

        if ($request->has('homepage')) {
            $query->where('homepage', $request->get('homepage'));
        }

        if ($request->has('category')) {
            $query->where('category_id', $request->get('category'));
        }

        if ($request->has('user')) {
            $query->where('user_id', $request->get('user'));
        }

        return $query;
    }

    public function getSortDescAttribute()
    {
        return $this->DescFormatter($this->description);
    }

    private function DescFormatter($in)
    {
        $out = strlen($in) > 100 ? substr($in,0,100)."..." : $in;
        return $out;
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
