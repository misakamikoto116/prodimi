@extends('layouts.main')  

@section('title','Struktur Organisasi')

@section('contents')

  <!--/ Intro Single star /-->
  <section class="intro-single">
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-lg-8">
          <div class="title-single-box">
            <h1 class="title-single">Struktur Organisasi</h1>
          </div>
        </div>
        <div class="col-md-12 col-lg-4">
          <nav aria-label="breadcrumb" class="breadcrumb-box d-flex justify-content-lg-end">
            <ol class="breadcrumb">
              <li class="breadcrumb-item">
                <a href="{{ route('index') }}">Beranda</a>
              </li>
              <li class="breadcrumb-item active" aria-current="page">
                Struktur Organisasi
              </li>
            </ol>
          </nav>
        </div>
      </div>
    </div>
  </section>
  <!--/ Intro Single End /-->

  <!-- visi,misi,tujuan -->
  <section style="background-color: #f3f3f3;" class="ftco-section section-t8" id="lovestory-section">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <ul class="timeline animate-box">
            @foreach($struktur_organisasis as $key => $struktur_organisasi)
              <li class="{{ $key % 2 === 0 ?  '' : 'timeline-inverted'}} animate-box">
                <div class="timeline-badge" style="background-image:url(front/img/bgcircle.jpg);"> </div>
                <div class="timeline-panel ftco-animate {{ $key % 2 === 0 ?  '' : 'text-md-right'}}">
                  <div class="overlay"></div>
                  <div class="timeline-heading">
                    <h3 class="timeline-title">
                      <span class="fa fa-quote-left color-b"></span>
                      {{ $struktur_organisasi->name }}
                      <span class="fa fa-quote-right color-b"></span>
                    </h3>
                  </div>
                  <div class="timeline-body">
                    <p>Jabatan : {{ $struktur_organisasi->jabatan }}</p>
                    <p>NIM : {{ $struktur_organisasi->nim }}</p>
                  </div>
                </div>
              </li>
            @endforeach
          </ul>
        </div>
      </div>
    </div>
  </section>
  <!-- visi,misi,tujuan -->

@endsection