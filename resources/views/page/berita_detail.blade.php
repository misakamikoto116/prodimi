@extends('layouts.main')  

@section('title', $blog->title)

@section('contents')
<section style="padding-top:8rem">
  <div class="container">

    <div class="row">

      <!-- Post Content Column -->
      <div class="col-md-8">

        <!-- Title -->
        <h1 class="mt-4">{{ $blog->title }} </h1>

        <!-- Author -->
        <p class="lead">
          by
          <a href="{{ url('berita?user=' . $blog->user_id) }}">{{ $blog->user->name }}</a>
          | Category : <a href="{{ url('berita?category=' . $blog->category) }}">{{ $blog->category->name }}</a>
        </p>

        <hr>

        <!-- Date/Time -->
        <p>Posted on {{ $blog->created_at->format('F d') . ', ' . $blog->created_at->format('Y') . ' at ' . $blog->created_at->format('h:i A') }}</p>

        <hr>

        <!-- Preview Image -->
        <img class="img-fluid rounded" src="{{ asset('storage/blogs/' . $blog->thumbnail) }}" alt="">

        <hr>


        {!! $blog->description !!}

        <hr>

      </div>

      <!-- Sidebar Widgets Column -->
      <div class="col-md-4">

        <div class="widget-sidebar widget-tags post-box my-4">
          <h5 class="sidebar-title">Kategori</h5>
          <div class="sidebar-content">
            <ul>
                @foreach($categories as $category)
                    <li>
                        <a href="{{ url('berita?category=' . $category->id) }}">{{ $category->name }}</a>
                    </li>
                @endforeach
            </ul>
          </div>
        </div>

        <!-- Side Widget -->
        <div class="card my-4">
          <h5 class="card-header">Lainnya Dari {{ $blog->user->name }}</h5>
          <div class="card-body">
            @foreach($blog->user->blogs()->orderBy('created_at', 'DESC')->take(4)->get() as $userBlog)
            <hr>
              <div>
                <a href="{{ route('berita.detail', $userBlog->id) }}"><button class="btn btn-danger btn-block text-left btn-arrow-right"> {{ $userBlog->title }} </button></a>
              </div>
            <hr>
            @endforeach
          </div>
        </div>

      </div>

    </div>
  <!-- /.row -->

  </div>
</section>

  <!--/ Agents End /-->
           
@endsection