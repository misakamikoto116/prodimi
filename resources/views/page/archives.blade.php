@extends('layouts.main')  

@section('title', 'Archive')

@section('contents')
<section class="intro-single">
  <div class="container">
      <div class="row">
          <div class="col-md-12 col-lg-8">
              <div class="title-single-box">
                  <h1 class="title-single">Archive Tahun {{ $TA }}</h1>
              </div>
          </div>
          <div class="col-md-12 col-lg-4">
              <nav aria-label="breadcrumb" class="breadcrumb-box d-flex justify-content-lg-end">
                  <ol class="breadcrumb">
                      <li class="breadcrumb-item">
                          <a href="{{ route('index') }}">Beranda</a>
                      </li>
                      <li class="breadcrumb-item active" aria-current="page">
                          Archive
                      </li>
                  </ol>
              </nav>
          </div>
      </div>
  </div>
</section>


<section class="section-t3">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            <div class="d-flex">
              <div class="mr-auto">
                Kurikulum
              </div>
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <table class="table table-bordered table-striped">
              <thead class="thead-light">                  
                <tr>
                  <th style="width: 10px">#</th>
                  <th>Tahun</th>
                  <th>File</th>
                </tr>
              </thead>
              <tbody>
                @if($Ks->count() > 0)
                  @foreach($Ks as $K)
                    <tr>
                      <td>{{ $loop->iteration }}</td>
                      <td>{{ $K->tahun }}</td>
                      <td width="5%"><a href="{{ asset('storage/kurikulum/' . $K->file) }}" target="_blank"><button class="btn btn-info">Download</button></a></td>
                    </tr>
                  @endforeach
                @else
                  <tr>
                    <td colspan="3" class="text-center">Tidak Ada Kurikulum</td>
                  </tr>
                @endif
              </tbody>
            </table>
          </div>
        </div>
        <!-- /.card -->
      </div>
    </div>
    <hr>
    <br>
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            <div class="d-flex">
              <div class="mr-auto">
                Kalender Akademik
              </div>
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <table class="table table-bordered table-striped">
              <thead class="thead-light">                  
                <tr>
                  <th style="width: 10px">#</th>
                  <th>Tahun</th>
                  <th>File</th>
                </tr>
              </thead>
              <tbody>
                @if($KAs->count() > 0)
                  @foreach($KAs as $KA)
                    <tr>
                      <td>{{ $loop->iteration }}</td>
                      <td>{{ $KA->tahun }}</td>
                      <td width="5%"><a href="{{ asset('storage/kalender-akademik/' . $KA->file) }}" target="_blank"><button class="btn btn-info">Download</button></a></td>
                    </tr>
                  @endforeach
                @else
                  <tr>
                    <td colspan="3" class="text-center">Tidak Ada Kalender Akademik</td>
                  </tr>
                @endif
              </tbody>
            </table>
          </div>
        </div>
        <!-- /.card -->
      </div>
    </div>
    <hr>
    <br>
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            <div class="d-flex">
              <div class="mr-auto">
                Jadwal Perkuliahan
              </div>
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <table class="table table-bordered table-striped">
              <thead class="thead-light">                  
                <tr>
                  <th style="width: 10px">#</th>
                  <th>Tahun</th>
                  <th>File</th>
                </tr>
              </thead>
              <tbody>
                @if($JPs->count() > 0)
                  @foreach($JPs as $JP)
                    <tr>
                      <td>{{ $loop->iteration }}</td>
                      <td>{{ $JP->tahun }}</td>
                      <td width="5%"><a href="{{ asset('storage/jadwal-perkuliahan/' . $JP->file) }}" target="_blank"><button class="btn btn-info">Download</button></a></td>
                    </tr>
                  @endforeach
                @else
                  <tr>
                    <td colspan="3" class="text-center">Tidak Ada Jadwal Perkuliahan</td>
                  </tr>
                @endif
              </tbody>
            </table>
          </div>
        </div>
        <!-- /.card -->
      </div>
    </div>
    <hr>
    <br>
  </div>
</section>

  <!--/ Agents End /-->
           
@endsection