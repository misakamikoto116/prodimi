@extends('layouts.main')  

@section('title','Galeri')

@section('contents')
<section class="intro-single">
  <div class="container">
      <div class="row">
          <div class="col-md-12 col-lg-8">
              <div class="title-single-box">
                  <h1 class="title-single">Galeri</h1>
              </div>
          </div>
          <div class="col-md-12 col-lg-4">
              <nav aria-label="breadcrumb" class="breadcrumb-box d-flex justify-content-lg-end">
                  <ol class="breadcrumb">
                      <li class="breadcrumb-item">
                          <a href="{{ route('index') }}">Beranda</a>
                      </li>
                      <li class="breadcrumb-item active" aria-current="page">
                          Galeri
                      </li>
                  </ol>
              </nav>
          </div>
      </div>
  </div>
</section>


<section class="section-t3">
  <div class="container">
    <div class="row">
      <!-- <div class="col-md-8">
      </div> -->
      <div class="col-md-8">
        <div class="row">
          @foreach($galleries as $gallery)
            <div class="col-md-4">
              <div class="work-box">
                <a href="{{ asset('storage/galleries/' . $gallery->img) }}" data-lightbox="gallery-mf">
                  <div class="work-img">
                    <img src="{{ asset('storage/galleries/' . $gallery->img) }}" style="width:100%;" alt="" class="img-fluid">
                  </div>
                </a>
                <div class="work-content">
                  <div class="row">
                    <div class="col-sm-8">
                      <h2 class="w-title">{{ $gallery->name }}</h2>
                      <div class="w-more">
                        <a href="{{ url('galleries?category=' . $gallery->gallery_category_id) }}" class="w-ctegory">{{ $gallery->category->name }}</a> / <span class="w-date">{{ $gallery->created_at->format('d M Y') }}</span>
                      </div>
                    </div>
                  </div>
                </div>

              </div>
            </div>
          @endforeach
          </div>

          <!-- Pagination -->
          <div class="d-flex justify-content-center">
            {{ $galleries->appends(request()->all())->links() }}
          </div>
      </div>
      <div class="col-md-4">
          <div class="widget-sidebar widget-tags post-box">
              <h5 class="sidebar-title">Kategori</h5>
              <div class="sidebar-content">
                  <ul>
                      @foreach($categories as $category)
                          <li>
                              <a href="{{ url('galleries?category=' . $category->id) }}">{{ $category->name }}</a>
                          </li>
                      @endforeach
                  </ul>
              </div>
          </div>
      </div>
    </div>  
  </div>
</section>

  <!--/ Agents End /-->
           
@endsection