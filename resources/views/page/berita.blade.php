@extends('layouts.main')  

@section('title','Berita')

@section('contents')
<section class="intro-single">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-lg-8">
                <div class="title-single-box">
                    <h1 class="title-single">Berita</h1>
                </div>
            </div>
            <div class="col-md-12 col-lg-4">
                <nav aria-label="breadcrumb" class="breadcrumb-box d-flex justify-content-lg-end">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="{{ route('index') }}">Beranda</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">
                            Berita
                        </li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</section>
<!--/ Intro Single End /-->

<!-- bagi grid -->
<section class="section-t3">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                @foreach($blogs as $blog)
                    <div class="card mb-4">
                        <img class="card-img-top" src="{{ asset('storage/blogs/' . $blog->thumbnail) }}" style="height: 600px;" alt="Card image cap">
                        <div class="card-body">
                            <h2 class="title">{{ $blog->title }}</h2>
                            <p class="card-text">
                                {!! $blog->sort_desc !!}
                            </p>
                            <a href="{{ route('berita.detail', $blog->id) }}" class="btn btn-prodi">Selengkapnya &rarr;</a>
                        </div>
                        <div class="card-footer text-muted justify-content-between">
                            <span><i class="fa fa-calendar"></i> {{ $blog->created_at->format('d F Y') }} </span>
                            <span>by <i class="fa fa-user"></i> <a href="{{ url('berita?user=' . $blog->user_id) }}">{{ $blog->user->name }}</a></span>
                        </div>
                    </div>
                @endforeach


                <!-- Pagination -->
                <div class="d-flex justify-content-center mt-4">
                    {{ $blogs->appends(request()->all())->links() }}
                </div>
                <!-- <ul class="pagination justify-content-center mb-4">
                    <li class="page-item">
                        <a class="btn btn-secondary disabled" href="#">&bigstar; Terbaru</a>
                    </li>
                    &nbsp;
                    <li class="page-item ">
                        <a class="btn btn-prodi" href="#">Lainnya &rarr;</a>
                    </li>
                </ul> -->
            </div>

            <div class="col-md-4">
                <div class="widget-sidebar widget-tags post-box">
                    <h5 class="sidebar-title">Kategori</h5>
                    <div class="sidebar-content">
                        <ul>
                            @foreach($categories as $category)
                                <li>
                                    <a href="{{ url('berita?category=' . $category->id) }}">{{ $category->name }}</a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>

            <!-- <div class="col-lg-4"> 
                <div class="property-summary mb-5">
                    <div class="col-lg-12">
                        <div class="title-box-d ">
                            <h3 class="title-d">Kategori</h3>
                        </div>
                    </div>
                    <div class="summary-list">
                        <div class="col-lg-12">
                            <ul class="list-unstyled ">
                                <li class="d-flex mt-2">
                                    <strong><i class="fa fa-chevron-circle-right color-b">&nbsp;</i><a href="#">Web
                                            Design</a>
                                    </strong>
                                </li>
                                <li class="d-flex mt-2">
                                    <strong><i class="fa fa-chevron-circle-right color-b">&nbsp;</i><a
                                            href="#">HTML</a>
                                    </strong>
                                </li>
                                <li class="d-flex mt-2">
                                    <strong><i class="fa fa-chevron-circle-right color-b">&nbsp;</i><a
                                            href="#">Seminar</a>
                                    </strong>
                                </li>
                                <li class="d-flex mt-2">
                                    <strong><i class="fa fa-chevron-circle-right color-b">&nbsp;</i><a
                                            href="#">Himpunan</a>
                                    </strong>
                                </li>
                                <li class="d-flex mt-2">
                                    <strong><i class="fa fa-chevron-circle-right color-b">&nbsp;</i><a
                                            href="#">Perkuliahan</a>
                                    </strong>
                                </li>
                                <li class="d-flex mt-2">
                                    <strong><i class="fa fa-chevron-circle-right color-b">&nbsp;</i><a
                                            href="#">CSS</a>
                                    </strong>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                
                <div class="card my-4">
                    <h5 class="card-header">Side Widget</h5>
                    <div class="card-body">
                        You can put anything you want inside of these side widgets. They are easy to use, and
                        feature the new Bootstrap 4 card containers!
                    </div>
                </div>

            </div>  -->
        </div>
    </div>
</section>
@endsection