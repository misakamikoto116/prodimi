@extends('layouts.main')  

@section('title','Dosen')

@section('contents')
<section class="intro-single">
  <div class="container">
      <div class="row">
          <div class="col-md-12 col-lg-8">
              <div class="title-single-box">
                  <h1 class="title-single">Dosen</h1>
              </div>
          </div>
          <div class="col-md-12 col-lg-4">
              <nav aria-label="breadcrumb" class="breadcrumb-box d-flex justify-content-lg-end">
                  <ol class="breadcrumb">
                      <li class="breadcrumb-item">
                          <a href="{{ route('index') }}">Beranda</a>
                      </li>
                      <li class="breadcrumb-item active" aria-current="page">
                          Dosen
                      </li>
                  </ol>
              </nav>
          </div>
      </div>
  </div>
</section>

<div class="container">
  <div class="row">
    <div class="col-md-12">
      <section id="team" class="section-news section-t8">
        <div class="container">
          <div class="row">
            @foreach($dosens as $dosen)
              <div class="col-md-4">
                <div class="card-box-b card-shadow news-box member">
                  <div class="img-box-b">
                    <img src="{{ asset('storage/dosens/' . $dosen->img) }}" alt="" style="height:350px;width:100%;" class="img-b img-fluid">
                  </div>
                  <div class="member-info">
                    <div class="member-info-content">
                      <h4>{{ $dosen->name }}</h4>
                      <span>{{ $dosen->kejuruan }}</span>
                    </div>
                  </div>
                </div>
              </div>
            @endforeach
          </div>
          <div class="d-flex justify-content-center mt-4">
            {{ $dosens->appends(request()->all())->links() }}
          </div>
        </div>
      </section>
    </div>
  </div>  
</div>

  <!--/ Agents End /-->
           
@endsection