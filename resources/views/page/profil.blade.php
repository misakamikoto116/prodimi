@extends('layouts.main')  

@section('title','Profil')

@section('contents')

  <!--/ Intro Single star /-->
  <section class="intro-single">
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-lg-8">
          <div class="title-single-box">
            <h1 class="title-single">Program Studi Manajemen Informatika</h1>
          </div>
        </div>
        <div class="col-md-12 col-lg-4">
          <nav aria-label="breadcrumb" class="breadcrumb-box d-flex justify-content-lg-end">
            <ol class="breadcrumb">
              <li class="breadcrumb-item">
                <a href="{{ route('index') }}">Beranda</a>
              </li>
              <li class="breadcrumb-item active" aria-current="page">
                Profil
              </li>
            </ol>
          </nav>
        </div>
      </div>
    </div>
  </section>
  <!--/ Intro Single End /-->

  <!--/ About Star /-->
  <section class="section-about">
    <div class="container">
      <div class="row">
        <div class="col-md-12 section-t8">
          <div class="row">
            <div class="col-md-6 col-lg-3">
              <img src="{{ !empty($sambutan->img) ? asset('storage/sambutan/' . $sambutan->img) : 'front/img/profil/kaprodi.jpg' }}" alt="" class="img-fluid">
              <h5 class="title-d mt-4">{{ $sambutan->name }}</h5>
            </div>
            <div class="col-lg-2  d-none d-lg-block">
              <div class="title-vertical d-flex justify-content-start">
                <span>Sambutan </span>
              </div>
            </div>
            <div class="col-md-6 col-lg-7 section-md-t3 ">
              <div class="title-box-d">
                <h3 class="title-d">Kepala Program Studi Manejemen Informatika </h3>
              </div>
              <div class="text-justify">
                <p class="color-text-a">
                  {!! $sambutan->quote !!}
                </p>
                </divc>
              </div>
            </div>
          </div>
        </div>
      </div>
  </section>
  <!--/ About End /-->

  <!-- visi,misi,tujuan -->
  <section style="background-color: #f3f3f3;" class="ftco-section section-t8" id="lovestory-section">
    <div class="container">
      <div class="row justify-content-center pb-5">
        <div class="col-md-12 text-center heading-section ftco-animate">
          <h2 class="mb-3">Profil Prodi Manajemen Informatika</h2>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <ul class="timeline animate-box">
            <li class="animate-box">
              <div class="timeline-badge" style="background-image:url(front/img/bgcircle.jpg);"> </div>
              <div class="timeline-panel ftco-animate text-md-right">
                <div class="overlay"></div>
                <div class="timeline-heading">
                  <h3 class="timeline-title">
                    <span class="fa fa-quote-left color-b"></span>
                    Visi
                    <span class="fa fa-quote-right color-b"></span>
                  </h3>
                </div>
                <div class="timeline-body">
                  <p>”{{ $visi }}”.</p>
                </div>
              </div>
            </li>
            <li class="timeline-inverted animate-box">
              <div class="timeline-badge" style="background-image:url(front/img/bgcircle.jpg);"> </div>
              <div class="timeline-panel ftco-animate">
                <div class="overlay overlay-2"></div>
                <div class="timeline-heading">
                  <h3 class="timeline-title">
                    <span class="fa fa-quote-left color-b"></span>
                    Misi
                    <span class="fa fa-quote-right color-b"></span>
                  </h3>
                </div>
                <div class="timeline-body text-justify">
                  {{ $misi }}
                </div>
              </div>
            </li>
            <li class="animate-box">
              <div class="timeline-badge" style="background-image:url(front/img/bgcircle.jpg);"> </div>
              <div class="timeline-panel ftco-animate text-md-right">
                <div class="overlay"></div>
                <div class="timeline-heading">
                  <h3 class="timeline-title">
                    <span class="fa fa-quote-left color-b"></span>
                    Tujuan
                    <span class="fa fa-quote-right color-b"></span>
                  </h3>
                </div>
                <div class="timeline-body text-justify">
                  {{ $tujuan }}
                </div>
              </div>
            </li>
            <li class="timeline-inverted animate-box">
              <div class="timeline-badge" style="background-image:url(front/img/bgcircle.jpg);"> </div>
              <div class="timeline-panel ftco-animate">
                <div class="overlay overlay-2"></div>
                <div class="timeline-heading">
                  <h3 class="timeline-title">
                    <span class="fa fa-quote-left color-b"></span>
                    Tujuan Pendidikan
                    <span class="fa fa-quote-right color-b"></span>
                  </h3>
                </div>
                <div class="timeline-body text-justify">
                  {{ $tujuan_pendidikan }}
                </div>
              </div>
            </li>
            <li class="animate-box">
              <div class="timeline-badge" style="background-image:url(front/img/bgcircle.jpg);"> </div>
              <div class="timeline-panel ftco-animate text-md-right">
                <div class="overlay"></div>
                <div class="timeline-heading">
                  <h3 class="timeline-title">
                    <span class="fa fa-quote-left color-b"></span>
                    Profil Pendidikan
                    <span class="fa fa-quote-right color-b"></span>
                  </h3>
                </div>
                <div class="timeline-body text-justify">
                  {{ $profil_pendidikan }}
                </div>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </section>
  <!-- visi,misi,tujuan -->

  <!-- dosen prodi -->

  <section id="team" class="section-news section-t8">
    <div class="container">
      <div class="row justify-content-center pb-5">
        <div class="col-md-12 text-center heading-section ftco-animate">
          <h2 class="mb-3">Dosen Prodi</h2>
        </div>
      </div>
      <div id="new-carousel" class="owl-carousel owl-theme">
        @foreach($dosens as $dosen)
          <div class="carousel-item-c wow fadeInUp">
            <div class="card-box-b card-shadow news-box member">
              <div class="img-box-b">
                <img src="{{ asset('storage/dosens/' . $dosen->img) }}" alt="" style="height:350px;width:100%;" class="img-b img-fluid">
              </div>
              <div class="member-info">
                <div class="member-info-content">
                  <h4>{{ $dosen->name }}</h4>
                  <span>{{ $dosen->kejuruan }}</span>
                </div>
              </div>
            </div>
          </div>
        @endforeach
      </div>
    </div>
  </section>
  <!-- dosen prodi -->

@endsection