@extends('layouts.main')  

@section('title','Alumni')

@section('contents')
<section class="intro-single">
  <div class="container">
      <div class="row">
          <div class="col-md-12 col-lg-8">
              <div class="title-single-box">
                  <h1 class="title-single">Alumni</h1>
              </div>
          </div>
          <div class="col-md-12 col-lg-4">
              <nav aria-label="breadcrumb" class="breadcrumb-box d-flex justify-content-lg-end">
                  <ol class="breadcrumb">
                      <li class="breadcrumb-item">
                          <a href="{{ route('index') }}">Beranda</a>
                      </li>
                      <li class="breadcrumb-item active" aria-current="page">
                          Alumni
                      </li>
                  </ol>
              </nav>
          </div>
      </div>
  </div>
</section>


<section class="section-t3">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            <div class="d-flex">
              @include('admin.components.search', ['placeholder' => 'Nama, Angkatan, Job'])
            </div>
            <div class="mt-2 mb-2">
              @if(Request::has('verif') || Request::has('q'))
                  <a class="label label-info" href="{{ url('alumni') }}">Semua</a>
                  <div class="clearfix"></div>
                  <br>
              @endif
            </div>
            <a class="label label-primary" href="{{ url('alumni?verif=1') }}">Terverifikasi</a>
            <a class="label label-danger" href="{{ url('alumni?verif=0') }}">Belum Diverifikasi</a>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered table-striped">
                <thead class="thead-light">                  
                  <tr>
                    <th style="width: 10px">#</th>
                    <th>Nama</th>
                    <th>Foto</th>
                    <th>Angkatan</th>
                    <th>Pekerjaan</th>
                  </tr>
                </thead>
                <tbody>
                  @if($alumnis->count() > 0)
                    @foreach($alumnis as $alumni)
                      <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $alumni->nama }}</td>
                        <td>
                          <img src="{{ asset('storage/alumnis/' . $alumni->img) }}" class="img-thumbnail" style="height: 150px;width: 150px" alt="">
                        </td>
                        <td>{{ $alumni->angkatan }}</td>
                        <td>{{ $alumni->job->name }}</td>
                      </tr>
                    @endforeach
                  @else
                    <tr>
                      <td colspan="3" class="text-center">Tidak Ada Data Alumni</td>
                    </tr>
                  @endif
                </tbody>
              </table>
            </div>
          </div>
          <div class="card-footer d-flex justify-content-end">
            {{ $alumnis->appends(request()->all())->links() }}
          </div>
        </div>
        <!-- /.card -->
      </div>
    </div>
  </div>
</section>

  <!--/ Agents End /-->
           
@endsection