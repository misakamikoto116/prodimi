@extends('layouts.main')  

@section('title','Beranda')

@section('contents')

  <!--/ Carousel Star /-->
  <div class="intro intro-carousel">
    <div id="carousel" class="owl-carousel owl-theme">
      @foreach($homepageBlogs as $blog)
        <div class="carousel-item-a intro-item bg-image" style="background-image: url({{asset('storage/blogs/' . $blog->thumbnail)}})">
          <div class="overlay overlay-a"></div>
          <div class="intro-content display-table">
            <div class="table-cell">
              <div class="container">
                <div class="row">
                  <div class="col-lg-8">
                    <div class="intro-body">
                      <p class="intro-title-top">
                        <br> </p>
                      <h1 class="intro-title mb-4">
                        <span class="color-b">{{ $blog->title }}</span>
                      <p class="intro-subtitle intro-price">
                        <a href="{{ route('berita.detail', $blog->id) }}"><span class="price-a">Selengkapnya</span></a>
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      @endforeach
    </div>
  </div>
  <!--/ Carousel end /-->

  <!--/ News Star /-->
  <section class="section-news section-tb85">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="title-wrap d-flex justify-content-between">
            <div class="title-box">
              <h2 class="title-a">Berita Terkini</h2>
            </div>
            <div class="title-link">
              <a href="{{ route('berita') }}">Pusat Berita
                <span class="ion-ios-arrow-forward"></span>
              </a>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        @foreach($lastBlogs as $lastBlog)
          <div class="col-md-4">
            <div class="card card-blog">
              <div class="card-img">
                <img src="{{ asset('storage/blogs/' . $lastBlog->thumbnail) }}" style="height:300px;width:100%;" alt="" class="img-fluid">
              </div>
              <div class="card-body">
                <div class="card-category-box">
                  <a href="{{ route('berita.detail', $lastBlog->id) }}"><button class="btn btn-prodi">Selengkapnya</button></a>
                </div>
                <h3 class="card-title"><span>{{ $lastBlog->title }}</span></h3>
                <p class="card-description">
                  {!! $lastBlog->sort_desc !!}
                </p>
              </div>
              <div class="card-footer">
                <div class="post-date">
                  <span class="fa fa-calendar"></span> {{ $lastBlog->created_at->format('d F Y') }}
                </div>
              </div>
            </div>
          </div>
        @endforeach
      </div>
    </div>
  </section>

  <!--/ News End /




  / Services Star  -->

  <div class="section-counter paralax-mf bg-image" style="background-image: url(front/img/counters-bg.jpg)">
    <div class="overlay-mf"></div>
    <div class="container">
      <div class="row">
        <div class="col-sm-4 ">
          <div class="counter-box pt-4 pt-md-0">
            <div class="counter-ico">
              <span class="ico-circle"><span class="fa fa-line-chart"></span></span>
            </div>
            <div class="counter-num">
              <p class="counter">Kurikulum</p>
              <span class="counter-text">{{ $kurikulum }}</span>
            </div>
          </div>
        </div>
        <div class="col-sm-4 ">
          <div class="counter-box pt-4 pt-md-0">
            <div class="counter-ico">
              <span class="ico-circle"><span class="fa fa-graduation-cap"></span></span>
            </div>
            <div class="counter-num">
              <p class="counter">Mahasiswa dan Alumni</p>
              <span class="counter-text">{{ $mahasiswa_dan_alumni }}</span>
            </div>
          </div>
        </div>
        <div class="col-sm-4">
          <div class="counter-box pt-4 pt-md-0">
            <div class="counter-ico">
              <span class="ico-circle"><span class="fa fa-handshake-o"></span></span>
            </div>
            <div class="counter-num">
              <p class="counter">Kerja Sama</p>
              <span class="counter-text">{{ $kerja_sama }}</span>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- 
  <section class="section-services section-t8">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="title-wrap d-flex justify-content-between">
            <div class="title-box">
              <h2 class="title-a">Our Services</h2>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-4">
          <div class="card-box-c foo">
            <div class="card-header-c d-flex">
              <div class="card-box-ico">
                <span class="fa fa-gamepad"></span>
              </div>
              <div class="card-title-c align-self-center">
                <h2 class="title-c">Lifestyle</h2>
              </div>
            </div>
            <div class="card-body-c">
              <p class="content-c">
                Sed porttitor lectus nibh. Cras ultricies ligula sed magna dictum porta. Praesent sapien massa,
                convallis a pellentesque
                nec, egestas non nisi.
              </p>
            </div>
            <div class="card-footer-c">
              <a href="#" class="link-c link-icon">Read more
                <span class="ion-ios-arrow-forward"></span>
              </a>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="card-box-c foo">
            <div class="card-header-c d-flex">
              <div class="card-box-ico">
                <span class="fa fa-usd"></span>
              </div>
              <div class="card-title-c align-self-center">
                <h2 class="title-c">Loans</h2>
              </div>
            </div>
            <div class="card-body-c">
              <p class="content-c">
                Nulla porttitor accumsan tincidunt. Curabitur aliquet quam id dui posuere blandit. Mauris blandit
                aliquet elit, eget tincidunt
                nibh pulvinar a.
              </p>
            </div>
            <div class="card-footer-c">
              <a href="#" class="link-c link-icon">Read more
                <span class="ion-ios-arrow-forward"></span>
              </a>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="card-box-c foo">
            <div class="card-header-c d-flex">
              <div class="card-box-ico">
                <span class="fa fa-home"></span>
              </div>
              <div class="card-title-c align-self-center">
                <h2 class="title-c">Sell</h2>
              </div>
            </div>
            <div class="card-body-c">
              <p class="content-c">
                Sed porttitor lectus nibh. Cras ultricies ligula sed magna dictum porta. Praesent sapien massa,
                convallis a pellentesque
                nec, egestas non nisi.
              </p>
            </div>
            <div class="card-footer-c">
              <a href="#" class="link-c link-icon">Read more
                <span class="ion-ios-arrow-forward"></span>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section> -->
  <!--/ Services End /-->

  <!--/ Property Star /-->

  <section id="work" class="portfolio-mf section-tb85 route">

    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="title-wrap d-flex justify-content-between">
            <div class="title-box">
              <h2 class="title-a">Galeri Kami</h2>
            </div>
            <div class="title-link">
              <a href="{{ route('galleries') }}">Pusat Galeri
                <span class="ion-ios-arrow-forward"></span>
              </a>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        @foreach($galleries as $gallery)
          <div class="col-md-4">
            <div class="work-box">
              <a href="{{ asset('storage/galleries/' . $gallery->img) }}" data-lightbox="gallery-mf">
                <div class="work-img">
                  <img src="{{ asset('storage/galleries/' . $gallery->img) }}" style="width:100%;" alt="" class="img-fluid">
                </div>
              </a>
              <div class="work-content">
                <div class="row">
                  <div class="col-sm-8">
                    <h2 class="w-title">{{ $gallery->name }}</h2>
                    <div class="w-more">
                      <a href="#" class="w-ctegory">{{ $gallery->category->name }}</a> / <span class="w-date">{{ $gallery->created_at->format('d M Y') }}</span>
                    </div>
                  </div>
                </div>
              </div>

            </div>
          </div>
        @endforeach
      </div>
    </div>
  </section>

  <!--/ Agents End /-->



  <!--/ Testimonials Star /-->
  <section class="section-testimonials paralax-m3 bg-image nav-arrow-a"
    style="background-image: url(front/img/overlay-bg1.jpg)">
    <div class="overlay-mf"></div>
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="title-wrap d-flex justify-content-between">
            <div class="title-box">
              <h2 class="title-a">Alumni</h2>
            </div>
          </div>
        </div>
      </div>
      <div id="testimonial-carousel" class="owl-carousel owl-arrow">
        @foreach($alumnis as $alumni)
          <div class="carousel-item-a">
            <div class="testimonials-box">
              <div class="row">
                <div class="col-md-12">
                  <div class="testimonial-ico">
                    <span class="ion-ios-quote"></span>
                  </div>
                  <div class="testimonials-content">
                    <p class="testimonial-text">
                      {{ $alumni->quote }}
                    </p>
                  </div>
                  <div class="testimonial-author-box">
                    <img src="{{ asset('storage/alumnis/' . $alumni->img) }}" class="testimonial-avatar" style="height:70px;">
                    <h5 class="testimonial-author">{{ $alumni->nama }}</h5>
                  </div>
                </div>
              </div>
            </div>
          </div>
        @endforeach
      </div>
      @if(Session::has('messages'))
        <div class="col-xs-12">
          <div class="alert alert-{{ Session::get('type') }} alert-relative alert-padding">
            <button type="button" class="close" data-dismiss="alert"
                      aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <div>
              {{ Session::get('messages') }}
            </div>
          </div>
        </div>
      @endif
      <div class="content" style="position:relative;">
        {{ Form::open(['route' => 'landing.alumni.store', 'method' => 'POST', 'files' => true]) }} 
          <div class="form-group">
            {{ Form::label('name', 'Name', ['class' => 'text-white']) }}
            {{ Form::text('name', old('name'), ['class' => 'form-control', 'id' => 'name', 'placeholder' => 'Masukkan Nama', 'required' => true]) }}
          </div>
          <div class="form-group">
            {{ Form::label('tahun_angkatan', 'Tahun Angkatan', ['class' => 'text-white']) }}
            {{ Form::number('tahun_angkatan', old('tahun_angkatan'), ['class' => 'form-control yearpickerLast', 'id' => 'tahun_angkatan', 'placeholder' => 'Masukkan Angkatan', 'required' => true]) }}
          </div>
          <div class="form-group">
            {{ Form::label('job', 'Pekerjaan', ['class' => 'text-white']) }}
            <select class="select_2 form-control" name="job_id" id="selected" required>
              <option value=""></option>
              @foreach($jobs as $job)
                <option value="{{ $job->id }}">{{ $job->name }}</option>
              @endforeach
            </select>
          </div>
          <div class="form-group">
            {{ Form::label('no_telepon', 'Nomor Telepon', ['class' => 'text-white']) }}
            {{ Form::number('no_telepon', old('no_telepon'), ['class' => 'form-control', 'id' => 'no_telepon', 'placeholder' => 'Masukkan Nomor Telepon', 'required' => true]) }}
          </div>
          <div class="form-group">
            {{ Form::label('quote', 'Quote', ['class' => 'text-white']) }}
            <textarea class="form-control" id="quote" name="quote" placeholder="Desription" style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{ old('quote') }}</textarea>
          </div>
          <label class="text-white">Upload Foto</label>
          <div class="form-group">
            {{ Form::file('img', ['class' => 'form-control pb-2', 'accept' => 'image/*', isset($data) ?? 'required' => true]) }}
          </div>
          <div class="form-group clearfix">
            {{ Form::submit('Save changes', ['class' => 'btn btn-success float-right']) }}
          </div>
        {{ Form::close() }}
      </div>
    </div>
  </section>
  <!--/ Testimonials End /-->

           
@endsection