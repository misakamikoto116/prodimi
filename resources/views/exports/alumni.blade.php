<table>
    <thead>
    <tr>
        <th>No</th>
        <th>Nama</th>
        <th>Angkatan</th>
        <th>Pekerjaan</th>
        <th>Nomor Telepon</th>
    </tr>
    </thead>
    <tbody>
    @foreach($datas as $data)
        <tr>
            <td align="left">{{ $loop->iteration }}</td>
            <td align="left">{{ $data->nama }}</td>
            <td align="left">{{ $data->angkatan }}</td>
            <td align="left">{{ $data->job->name }}</td>
            <td align="left">{{ $data->telp }}</td>
        </tr>
    @endforeach
    </tbody>
</table>