 <section class="section-footer">
    <div class="container">
      <div class="row">
        <div class="col-sm-12 col-md-5">
          <div class="widget-a">
            <div class="w-header-a">
              <h3 class="w-title-a text-brand">Prodi<span class="color-b">MI</span></h3>
            </div>
            <div class="contact-map box">
              <div id="map" class="contact-map">
                <iframe
                  src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3989.687663820297!2d117.14743843069475!3d-0.4636900784448137!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x2208693da058121c!2sSTMIK%20Widya%20Cipta%20Dharma!5e0!3m2!1sid!2sid!4v1576838806984!5m2!1sid!2sid"
                  width="100%" height="200" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
              </div>
            </div>
            <div class="w-body-a">
              <p class="w-text-a color-text-a">
                Alamat: Jl. Prof. M. Yamin No. 25 Samarinda Kalimantan Timur 75123
              </p>
            </div>
            <div class="w-footer-a">
              <ul class="list-unstyled">
                <li class="color-a">
                  <span class="color-text-a">Phone :</span> 0541-00000</li>
                <li class="color-a">
                  <span class="color-text-a">Email :</span> contact@example.com</li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-sm-12 col-md-3 section-md-t3">
          <div class="widget-a">
            <div class="w-header-a">
              <h3 class="w-title-a text-brand">Tautan</h3>
            </div>
            <div class="w-body-a">
              <div class="w-body-a">
                <ul class="list-unstyled">
                  <li class="item-list-a">
                    <i class="fa fa-angle-right"></i> <a href="http://www.wicida.ac.id/">STMIK WICIDA</a>
                  </li>
                  <li class="item-list-a">
                    <i class="fa fa-angle-right"></i> <a href="https://baak.wicida.ac.id/">BAAK WICIDA</a>
                  </li>
                  <li class="item-list-a">
                    <i class="fa fa-angle-right"></i> <a href="https://forlap.ristekdikti.go.id/">FORLAP DIKTI</a>
                  </li>
                </ul>
                
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-12 col-md-4 section-md-t3">
          <div class="widget-a">
            <div class="w-header-a">
              <h3 class="w-title-a text-brand">Bantuan & Support</h3>
            </div>
            <div class="w-body-a">
              <ul class="list-unstyled">
                <li class="item-list-a">
                  <i class="fa fa-angle-right"></i> <a href="#">Berita</a>
                </li>
                <li class="item-list-a">
                  <i class="fa fa-angle-right"></i> <a href="#">Galeri</a>
                </li>
                <li class="item-list-a">
                  <i class="fa fa-angle-right"></i> <a href="#">Download</a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <footer>
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="socials-a">
            <ul class="list-inline">
              <li class="list-inline-item">
                <a href="#">
                  <i class="fa fa-facebook" aria-hidden="true"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="#">
                  <i class="fa fa-twitter" aria-hidden="true"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="#">
                  <i class="fa fa-instagram" aria-hidden="true"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="#">
                  <i class="fa fa-pinterest-p" aria-hidden="true"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="#">
                  <i class="fa fa-dribbble" aria-hidden="true"></i>
                </a>
              </li>
            </ul>
          </div>
          <div class="copyright-footer">
            <p class="copyright color-text-a">
              &copy; Copyright
              <span class="color-a">Prodi<span class="color-b">MI</span></span> All Rights Reserved.
            </p>
          </div>
          <div class="credits">
            <!--
              All the links in the footer should remain intact.
              You can delete the links only if you purchased the pro version.
              Licensing information: https://bootstrapmade.com/license/
              Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=EstateAgency
            -->
            Designed by <a href="https://www.instagram.com/muh.afif_/">1731005</a>
          </div>
        </div>
      </div>
    </div>
  </footer>