<!DOCTYPE html>
<html lang="en">

@include('layouts.head')

<body>

  
  <!--/ Form Search Star /-->
  @include('layouts.header')
  <!--/ Nav End /-->
  @yield('contents')
  <!--/ footer Star /-->
  @include('layouts.footer')
  <!--/ Footer End /-->

  @include('layouts.setting')

  <!--End of Tawk.to Script-->
  <!--End of Tawk.to Script-->
</body>

</html>