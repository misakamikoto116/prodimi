  <!-- JavaScript Libraries -->
  <script src="{{ asset('front/lib/counterup/counterup.min.js') }}"></script>
  <script src="{{ asset('front/lib/jquery/jquery.min.js') }}"></script>
  <script src="{{ asset('front/lib/jquery/jquery-migrate.min.js') }}"></script>
  <script src="{{ asset('front/lib/popper/popper.min.js') }}"></script>
  <script src="{{ asset('front/lib/bootstrap/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('front/lib/easing/easing.min.js') }}"></script>
  <script src="{{ asset('front/lib/owlcarousel/owl.carousel.min.js') }}"></script>
  <script src="{{ asset('front/lib/scrollreveal/scrollreveal.min.js') }}"></script>
  <script src="{{ asset('front/lib/lightbox/js/lightbox.min.js') }}"></script>
  <script src="{{ asset('front/lib/isotope/isotope.pkgd.min.js') }}"></script>
  <!-- Contact Form JavaScript File -->
  <script src="{{ asset('front/contactform/contactform.js') }}"></script>
  <!-- datepicker -->
  <script src="{{ asset('back/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
  <!-- Select2  -->
  <script src="{{ asset('back/bower_components/select2/dist/js/select2.min.js') }}"></script>

  <!-- Template Main Javascript File -->
  <script src="{{ asset('front/js/main.js') }}"></script>
  <!--Start of Tawk.to Script-->
  <!--Start of Tawk.to Script-->
  <script type="text/javascript">
    var Tawk_API = Tawk_API || {},
      Tawk_LoadStart = new Date();
    (function () {
      var s1 = document.createElement("script"),
        s0 = document.getElementsByTagName("script")[0];
      s1.async = true;
      s1.src = 'https://embed.tawk.to/5dbd45b4e4c2fa4b6bd9a26d/default';
      s1.charset = 'UTF-8';
      s1.setAttribute('crossorigin', '*');
      s0.parentNode.insertBefore(s1, s0);
    })();
  </script>

  <script>
    // Datetimepicker
    $('.yearpicker').datepicker({
      format: 'yyyy', 
      icons: {
          time: 'fa fa-time',
          date: 'fa fa-calendar',
          up: 'fa fa-chevron-up',
          down: 'fa fa-chevron-down',
          previous: 'fa fa-chevron-left',
          next: 'fa fa-chevron-right',
          today: 'fa fa-screenshot',
          clear: 'fa fa-trash',
          close: 'fa fa-remove'
      },
      startView: "years",
      minViewMode: "years",
    });

    $('.select_2').select2({
      placeholder: "Pilih Data",
      allowClear: true
    });

    $('.yearpickerLast').datepicker({
      format: 'yyyy', 
      icons: {
          time: 'fa fa-time',
          date: 'fa fa-calendar',
          up: 'fa fa-chevron-up',
          down: 'fa fa-chevron-down',
          previous: 'fa fa-chevron-left',
          next: 'fa fa-chevron-right',
          today: 'fa fa-screenshot',
          clear: 'fa fa-trash',
          close: 'fa fa-remove'
      },
      startView: "years",
      minViewMode: "years",
      endDate: new Date(new Date().setDate(new Date().getDate() + 5))
    });
  </script>

<script type="text/javascript">var _Hasync= _Hasync|| [];
  _Hasync.push(['Histats.start', '1,4359777,4,9,110,60,00011110']);
  _Hasync.push(['Histats.fasi', '1']);
  _Hasync.push(['Histats.track_hits', '']);
  (function() {
  var hs = document.createElement('script'); hs.type = 'text/javascript'; hs.async = true;
  hs.src = ('//s10.histats.com/js15_as.js');
  (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(hs);
  })();</script>
  <noscript><a href="/" target="_blank"><img  src="//sstatic1.histats.com/0.gif?4359777&101" alt="" border="0"></a></noscript>