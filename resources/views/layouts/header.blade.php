<div class="click-closed"></div>

<div class="box-collapse">
    <div class="title-box-d">
      <h3 class="title-d">Pencarian</h3>
    </div>
    <span class="close-box-collapse right-boxed ion-ios-close"></span>
    <div class="box-collapse-wrap form">
      <form class="form-a">
        <div class="row">
          <div class="col-md-12 mb-2">
            <div class="form-group">
              <label for="Type">Pencarian</label>
              <input type="text" class="form-control form-control-lg form-control-a" placeholder="Masukan Data">
            </div>
          </div>
          <div class="col-md-12">
            <button type="submit" class="btn btn-b">Cari</button>
          </div>
        </div>
      </form>
    </div>
  </div>
  <!--/ Form Search End /-->

  <!--/ Nav Star /-->
  <nav class="navbar navbar-default navbar-trans navbar-expand-lg fixed-top">
    <div class="container">
      <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarDefault"
        aria-controls="navbarDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span></span>
        <span></span>
        <span></span>
      </button>
      <a class="navbar-brand text-brand" href="index.html">Prodi<span class="color-b">MI</span></a>
      <button type="button" class="btn btn-link nav-search navbar-toggle-box-collapse d-md-none" data-toggle="collapse"
        data-target="#navbarTogglerDemo01" aria-expanded="false">
        <span class="fa fa-search" aria-hidden="true"></span>
      </button>
      <div class="navbar-collapse collapse justify-content-center" id="navbarDefault">
        <ul class="navbar-nav">
          <li class="nav-item">
          <a class="nav-link {{ Request::path() === '/' ? 'active' : '' }}" href="{{ route('index') }}">Beranda</a>
          </li>
          <li class="nav-item">
            <a class="nav-link {{ Request::path() === 'profil' ? 'active' : '' }}" href="{{ route('profile') }}">Profil</a>
          </li>
          <li class="nav-item">
            <a class="nav-link {{ Request::segment(1) === 'berita'  ? 'active' : '' }}" href="{{ route('berita') }}">Berita</a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle {{ (Request::path() === 'archives' || Request::path() === 'dosen') ? 'active' : '' }}" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
              aria-haspopup="true" aria-expanded="false">
              Akademik
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item {{ Request::path() === 'archives' ? 'active' : '' }}" href="{{ route('archives') }}">Archive Akademik</a>
              <a class="dropdown-item {{ Request::path() === 'dosen' ? 'active' : '' }}" href="{{ route('dosen') }}">Daftar Dosen</a>
            </div>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle {{ (Request::path() === 'struktur-organisasi' || Request::path() === 'kegiatan' || Request::path() === 'alumni') ? 'active' : '' }}" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
              aria-haspopup="true" aria-expanded="false">
              Kemahasiswaan
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item {{ Request::path() === 'struktur-organisasi' ? 'active' : '' }}" href="{{ route('struktur-organisasi') }}">Struktur Organisasi</a>
              <a class="dropdown-item {{ Request::path() === 'kegiatan' ? 'active' : '' }}" href="{{ route('kegiatan') }}">Kegiatan</a>
              <a class="dropdown-item {{ Request::path() === 'alumni' ? 'active' : '' }}" href="{{ route('alumni') }}">Alumni</a>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link {{ Request::path() === 'galleries' ? 'active' : '' }}" href="{{ route('galleries') }}">Galeri</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>