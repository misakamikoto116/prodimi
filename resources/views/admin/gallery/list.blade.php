@extends('admin.layouts.main')
 
@section('titlehome','Galeri')  

@section('content') 
    @include('admin.gallery.templates.modal')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
        <h1>
            Galeri
            <small>List Galeri</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">List Gallery</li>
        </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="container-fluid">
          @if(Session::has('messages'))
            <div class="col-xs-12">
              <div class="alert alert-{{ Session::get('type') }} alert-relative alert-padding">
                <button type="button" class="close" data-dismiss="alert"
                          aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                <div>
                  {{ Session::get('messages') }}
                </div>
              </div>
            </div>
          @endif
            <div class="row">
              <div class="col-md-12">
                <div class="card">
                  <div class="card-header">
                  <div class="d-flex">
                    @include('admin.components.search', ['placeholder' => 'Nama, Kategori'])
                    <div class="ml-auto">
                      <a href="{{ route('gallery.create') }}"><button type="button" class="btn btn-primary btn-add-modal-gallery">Tambah Data</button></a>
                    </div>
                  </div>
                  </div>
                  <!-- /.card-header -->
                  <div class="card-body">
                    <div class="table-responsive">
                      <table class="table table-bordered table-striped">
                        <thead class="thead-light">                  
                          <tr>
                            <th style="width: 10px">#</th>
                            <th>Nama</th>
                            <th>Gambar</th>
                            <th>Kategori</th>
                            <th style="width: 40px">Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          @foreach($datas as $data)
                            <tr>
                              <td>{{ $loop->iteration }}</td>
                              <td>{{ $data->name }}</td>
                              <td>
                                <img src="{{ asset('storage/galleries/' . $data->img) }}" class="img-thumbnail" style="height: 150px;width: 150px" alt="">
                              </td>
                              <td>{{ $data->category->name }}</td>
                              <td class="d-flex">
                                <a href="{{ route('gallery.edit', $data->id ) }}"><button type="button" class="btn btn-success btn-edit-modal-gallery mr-1">Edit</button></a>
                                {{ Form::open(['route' => ['gallery.destroy', $data->id], 'onsubmit' => 'message(this, event)', 'data-message' => 'Ingin Menghapus Galeri ini', 'method' => 'DELETE']) }}
                                  <button class="btn btn-danger">Hapus</button>
                                {{ Form::close() }}
                              </td>
                            </tr>
                          @endforeach
                        </tbody>
                      </table>
                    </div>
                  </div>
                  <div class="card-footer d-flex justify-content-end">
                    {{ $datas->appends(request()->all())->links() }}
                  </div>
                </div>
                <!-- /.card -->
              </div>
              <!-- /.col -->
            </div>
          </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('js')
  <script>
    $('.btn-add-modal-gallery').click(function() {
      $('#modal-gallery').modal('show');
      $('#modal-gallery').find('#gallery-form').attr('action', $(this).data('url-add'));
      $('#modal-gallery').find('#name').val('');
      $('#modal-gallery').find('.modal-title').html('Tambah Data Kategori blog');
      $( "input[name='_method']").attr('disabled', true);
    });

    $('.btn-edit-modal-gallery').click(function() {
      $('#modal-gallery').modal('show');
      $('#modal-gallery').find('#gallery-form').attr('action', $(this).data('url-update'));
      $('#modal-gallery').find('#name').val($(this).data('name'));
      $('#modal-gallery').find('.modal-title').html('Edit Data Kategori blog');
      $( "input[name='_method']").attr('disabled', false);
    });
  </script>
@endsection