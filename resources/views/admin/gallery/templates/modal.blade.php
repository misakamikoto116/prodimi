<div class="modal fade" id="modal-archive" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      {{ Form::open(['route' => 'kurikulum.store', 'method' => 'PUT', 'id' => 'archive-form', 'files' => true]) }} 
        <div class="modal-body">
          <div class="form-group">
            {{ Form::label('tahun', 'Tahun') }}
            {{ Form::number('tahun', '', ['class' => 'form-control yearpicker', 'id' => 'tahun', 'placeholder' => 'Masukkan Tahun', 'required' => true]) }}
          </div>
          <label for="">Upload file Baru</label>
          <div class="form-group">
            {{ Form::file('img', ['class' => 'form-control pb-2']) }}
            <div class="border form-edit">
              <ul class="pt-2">
              <li>Tidak perlu pilih file jika tidak ingin mengubah file</li>
            </ul>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          {{ Form::submit('Save changes', ['class' => 'btn btn-primary']) }}
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      {{ Form::close() }}
    </div>
  </div>
</div>