@extends('admin.layouts.main')
 
@section('titlehome','Dashboard')  

@section('content') 
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
        <h1>
            Galeri
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('gallery.index') }}"><i class="fa fa-picture-o"></i> Data Gallery</a></li>
            <li class="active">{{ isset($data) ? 'Edit' : 'Tambah' }} Data Galeri</li>
        </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          @if(Session::has('messages'))
            <div class="col-xs-12">
              <div class="alert alert-{{ Session::get('type') }} alert-relative alert-padding">
                <button type="button" class="close" data-dismiss="alert"
                          aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                <div>
                  {{ Session::get('messages') }}
                </div>
              </div>
            </div>
          @endif
          <div class="row">
            <div class="col-md-12">
              <div class="card card-primary">
                <div class="card-header">
                  <h3 class="card-title">{{ isset($data) ? 'Edit' : 'Tambah' }} Data Galeri</h3>
                </div>
                @if(isset($data))
                  {{ Form::open(['route' => ['gallery.update', $data->id], 'method' => 'PUT', 'files' => true]) }} 
                @else
                  {{ Form::open(['route' => ['gallery.store'], 'method' => 'POST', 'files' => true]) }} 
                @endif

                {{ Form::token() }}
                <div class="card-body">
                  <div class="form-group">
                    {{ Form::label('nama', 'Nama') }}
                    {{ Form::text('nama', isset($data) ? old('name', $data->name) : old('name'), ['class' => 'form-control', 'id' => 'nama', 'placeholder' => 'Masukkan Nama' , 'required' => true]) }}
                  </div>

                  <div class="form-group">
                    <label for="name">Kategori</label>
                    <select class="select_2 form-control" name="category" id="selected" required>
                      <option value=""></option>
                      @foreach($categories as $category)
                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                      @endforeach
                    </select>
                  </div>
                  <label for="">Gambar</label>
                  @if(isset($data))
                    <div class="form-group">
                      <img src="{{ asset('storage/galleries/' . $data->img) }}" class="img-thumbnail" style="height: 300px;" alt="">
                    </div>
                  @endif

                  <label for="">Upload gambar Baru</label>
                  <div class="form-group">
                    {{ Form::file('img', ['class' => 'form-control pb-2', 'accept' => 'image/*', isset($data) ?? 'required' => true]) }}
                    <div class="border">
                      <ul class="pt-2">
                      @if(isset($data))
                        <li>Pilih gambar untuk mengubah gambar</li>
                        <li>Tidak perlu pilih gambar jika tidak ingin mengubah gambar</li>
                      @endif
                      <li>File harus gambar</li>
                    </ul>
                    </div>
                  </div>
                </div>
                <div class="card-footer clearfix">
                  <div class="col-12">
                    {{ Form::submit('Save changes', ['class' => 'btn btn-success float-right']) }}
                  </div>
                </div>
                {{ Form::close() }}
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
            </div>
          </div>
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('js')
  <script>
    @if(isset($data))
      $("#selected").val({{  $data->gallery_category_id }}).trigger('change');
    @endif
  </script>

@endsection