@extends('admin.layouts.main')
 
@section('titlehome','Blog')  

@section('content') 
  <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
      <h1>
          Blog
          <small>List Blog</small>
      </h1>
      <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active">List Blog</li>
      </ol>
      </section>

      <!-- Main content -->
      <section class="content">
        <div class="container-fluid">
        @if(Session::has('messages'))
          <div class="col-xs-12">
            <div class="alert alert-{{ Session::get('type') }} alert-relative alert-padding">
              <button type="button" class="close" data-dismiss="alert"
                        aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <div>
                {{ Session::get('messages') }}
              </div>
            </div>
          </div>
        @endif
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header">
                  <div class="d-flex">
                    @include('admin.components.search', ['placeholder' => 'Title, Pembuat, Kategori'])
                    <div class="ml-auto">
                      <a href="{{ route('blog.create') }}"><button type="button" class="btn btn-primary btn-add-modal-blog">Tambah Data</button></a>
                    </div>
                  </div>
                  <div class="mt-2 mb-2">
                    @if(Request::has('homepage') || Request::has('q'))
                        <a class="label label-info" href="{{ url('admin/blog') }}">Semua</a>
                        <div class="clearfix"></div>
                        <br>
                    @endif
                    <a class="label label-primary" href="{{ url('admin/blog?homepage=1') }}">Ditampilkan Dari Homepage</a>
                    <a class="label label-danger" href="{{ url('admin/blog?homepage=0') }}">Disembunyikan Dari Homepage</a>
                  </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                  <table class="table table-bordered table-striped">
                    <thead class="thead-light">                  
                      <tr>
                        <th style="width: 10px">#</th>
                        <th>Title</th>
                        <th>Thumbnail</th>
                        <th>Pembuat</th>
                        <th>Kategori</th>
                        <th>Homepage</th>
                        <th style="width: 40px">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($datas as $data)
                        <tr>
                          <td>{{ $loop->iteration }}</td>
                          <td>{{ $data->title }}</td>
                          <td>
                            <img src="{{ asset('storage/blogs/' . $data->thumbnail) }}" class="img-thumbnail" style="height: 150px;width: 150px" alt="">
                          </td>
                          <td>{{ $data->user->name }}</td>
                          <td>{{ $data->category->name }}</td>
                          <th>
                            {{ Form::open(['route' => ['blog.homepage', $data->id], 'method' => 'POST']) }}
                              <button class="btn btn-{{ $data->homepage ? 'success' : 'danger' }}" title="{{ $data->homepage ? 'Sembunyikan' : 'Tampilkan' }}">{{ $data->homepage ? 'Ditampilkan' : 'Disembunyikan' }}</button>
                            {{ Form::close() }}
                          </th>
                          <td class="d-flex">
                            <a href="{{ route('blog.edit', $data->id ) }}"><button type="button" class="btn btn-success btn-edit-modal-blog mr-1">Edit</button></a>
                            {{ Form::open(['route' => ['blog.destroy', $data->id], 'onsubmit' => 'message(this, event)', 'data-message' => 'Ingin Menghapus Blog ini', 'method' => 'DELETE']) }}
                              <button class="btn btn-danger">Hapus</button>
                            {{ Form::close() }}
                          </td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
                <div class="card-footer d-flex justify-content-end">
                  {{ $datas->appends(request()->all())->links() }}
                </div>
              </div>
              <!-- /.card -->
            </div>
            <!-- /.col -->
          </div>
        </div><!-- /.container-fluid -->
      </section>
      <!-- /.content -->
  </div>
@endsection