<aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="{{ asset('storage/users/' . auth()->user()->img) }}" class="img-circle" style="height:50px !important;" alt="User Image">
            </div>
            <div class="pull-left info">
              <p>{{ auth()->user()->name }}</p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN MENU</li>
            <li class="{!! \Request::segment(2) == 'dashboard' ? 'active' :  '' !!}"><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
            <li class="{!! \Request::segment(2) == 'user' ? 'active' :  '' !!}"><a href="{{ route('user.index') }}"><i class="fa fa-user"></i> <span>Users</span></a></li>
            <li class="{!! \Request::segment(2) == 'alumni' ? 'active' :  '' !!}"><a href="{{ route('alumni.index') }}"><i class="fa fa-graduation-cap"></i> <span>Alumni</span></a></li>
            <li class="{!! \Request::segment(2) == 'dosen' ? 'active' :  '' !!}"><a href="{{ route('dosen.index') }}"><i class="fa fa-users"></i> <span>Dosen Prodi</span></a></li>
            <li class="treeview {!! (\Request::segment(2) == 'kurikulum' || \Request::segment(2) == 'kalender-akademik' || \Request::segment(2) == 'jadwal-perkuliahan' || \Request::segment(2) == 'tahun-akademik' || \Request::segment(2) == 'berkas-kemahasiswaan') ? 'active' :  '' !!}">
              <a href="#">
                <i class="fa fa-file"></i> <span>Archives</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li class="{!! \Request::segment(2) == 'kurikulum' ? 'active' :  '' !!}"><a href="{{ route('kurikulum.index') }}"><i class="fa fa-circle-o"></i> Kurikulum</a></li>
                <li class="{!! \Request::segment(2) == 'kalender-akademik' ? 'active' :  '' !!}"><a href="{{ route('kalender-akademik.index') }}"><i class="fa fa-circle-o"></i> Kalender Akademik</a></li>
                <li class="{!! \Request::segment(2) == 'jadwal-perkuliahan' ? 'active' :  '' !!}"><a href="{{ route('jadwal-perkuliahan.index') }}"><i class="fa fa-circle-o"></i> Jadwal Perkuliahan</a></li>
                <li class="{!! \Request::segment(2) == 'berkas-kemahasiswaan' ? 'active' :  '' !!}"><a href="{{ route('berkas-kemahasiswaan.index') }}"><i class="fa fa-circle-o"></i> berkas Kemahasiswaan</a></li>
                <li class="{!! \Request::segment(2) == 'tahun-akademik' ? 'active' :  '' !!}"><a href="{{ route('tahun-akademik.index') }}"><i class="fa fa-circle-o"></i> Tahun Akademik</a></li>
              </ul>
            </li>
            <li class="treeview {!! (\Request::segment(2) == 'blog' || \Request::segment(2) == 'blog-categories') ? 'active' :  '' !!}">
              <a href="#">
                <i class="fa fa-newspaper-o"></i> <span>Blogs</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li class="{!! \Request::segment(2) == 'blog' ? 'active' :  '' !!}"><a href="{{ route('blog.index') }}"><i class="fa fa-circle-o"></i> Blog</a></li>
                <li class="{!! \Request::segment(2) == 'blog-categories' ? 'active' :  '' !!}"><a href="{{ route('blog-categories.index') }}"><i class="fa fa-circle-o"></i> Category</a></li>
              </ul>
            </li>
            <li class="treeview {!! (\Request::segment(2) == 'gallery' || \Request::segment(2) == 'gallery-categories') ? 'active' :  '' !!}">
              <a href="#">
                <i class="fa fa-picture-o"></i> <span>Galleries</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li class="{!! \Request::segment(2) == 'gallery' ? 'active' :  '' !!}"><a href="{{ route('gallery.index') }}"><i class="fa fa-circle-o"></i> Gallery</a></li>
                <li class="{!! \Request::segment(2) == 'gallery-categories' ? 'active' :  '' !!}"><a href="{{ route('gallery-categories.index') }}"><i class="fa fa-circle-o"></i> Category</a></li>
              </ul>
            </li>
            <li class="header">LANDING PAGE</li>
            <li class="{!! \Request::segment(2) == 'struktur-organisasi' ? 'active' :  '' !!}"><a href="{{ route('struktur-organisasi.index') }}"><i class="fa fa-list"></i> <span>Struktur Organisasi</span></a></li>
            <li class="{!! \Request::segment(2) == 'welcome' ? 'active' :  '' !!}"><a href="{{ route('welcome.index') }}"><i class="fa fa-file-text"></i> <span>Welcome text</span></a></li>
            <li class="{!! \Request::segment(2) == 'config' ? 'active' :  '' !!}"><a href="{{ route('config.index') }}"><i class="fa fa-cog"></i> <span>Configs</span></a></li>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>