<header class="main-header">
    <!-- Logo -->
    <a href="{{ url('admin/') }}" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>A</b>PRD</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Admin</b>PRODI</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src=" {{ asset('storage/users/' . auth()->user()->img) }}" class="user-image" alt="User Image">
              <span class="hidden-xs">{{ auth()->user()->name }}</span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src=" {{ asset('storage/users/' . auth()->user()->img) }}" class="img-circle" alt="User Image">

                <p>
                  {{  auth()->user()->name }}
                </p>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer d-flex">
                <div class="mr-auto">
                  <a href="{{ route('admin.get.profile') }}" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div>
                  <a href="{{ route('admin.get.password') }}" class="btn btn-default btn-flat">Password</a>
                </div>
                <div class="ml-auto">
                  <a href="{{ route('logout') }}" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>