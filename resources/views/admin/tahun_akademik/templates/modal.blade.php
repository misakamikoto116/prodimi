<div class="modal fade" id="modal-edit-tahun-akademik" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Edit Tahun Akademik</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      {{ Form::open(['route' => 'tahun-akademik.index', 'method' => 'PUT', 'id' => 'tahun-akademik-form']) }} 
        <div class="modal-body">
          <div class="form-group">
            {{ Form::label('tahun', 'Tahun') }}
            {{ Form::text('tahun', '', ['class' => 'form-control yearpicker', 'id' => 'tahun', 'placeholder' => 'Masukkan Tahun' , 'required' => true]) }}
          </div>
        </div>
        <div class="modal-footer">
          {{ Form::submit('Save changes', ['class' => 'btn btn-primary']) }}
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      {{ Form::close() }}
    </div>
  </div>
</div>