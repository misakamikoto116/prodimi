@extends('admin.layouts.main')
 
@section('titlehome','Tahun Akademik')  

@section('content') 
    @include('admin.tahun_akademik.templates.modal')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
        <h1>
            Tahun Akademik
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Tahun Akademik</li>
        </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="container-fluid">
          @if(Session::has('messages'))
            <div class="col-xs-12">
              <div class="alert alert-{{ Session::get('type') }} alert-relative alert-padding">
                <button type="button" class="close" data-dismiss="alert"
                          aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                <div>
                  {{ Session::get('messages') }}
                </div>
              </div>
            </div>
          @endif
            <div class="row">
              <div class="col-md-12">
                <div class="card">
                  <div class="card-header">
                    <h3 class="card-title">Data Tahun Akademik</h3>
                  </div>
                  <!-- /.card-header -->
                  <div class="card-body">
                    <table class="table table-bordered table-striped">
                      <thead class="thead-light">                  
                        <tr>
                          <th>Tahun</th>
                          <th style="width: 40px">Action</th>
                        </tr>
                      </thead>
                      <tbody>
                          <tr>
                            <td>{{ $data->tahun }}</td>
                            <td>
                              <button type="button" 
                                      class="btn btn-success btn-edit-modal-tahun-akademik" 
                                      data-tahun="{{ $data->tahun}}" 
                                      data-url-update="{{ route('tahun-akademik.update', $data->id) }}"
                              >Edit</button>
                            </td>
                          </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
                <!-- /.card -->
              </div>
              <!-- /.col -->
            </div>
          </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('js')
  <script>
    $('.btn-edit-modal-tahun-akademik').click(function() {
      $('#modal-edit-tahun-akademik').modal('show');
      $('#modal-edit-tahun-akademik').find('#tahun-akademik-form').attr('action', $(this).data('url-update'));
      $('#modal-edit-tahun-akademik').find('#tahun').val($(this).data('tahun'));
    });
  </script>
@endsection