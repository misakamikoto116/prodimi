@extends('admin.layouts.main')
 
@section('titlehome','Kategori Galeri')  

@section('content') 
    @include('admin.gallery_category.templates.modal')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
        <h1>
            Kategori Galeri
            <small>List Kategori Galeri</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">List Category Gallery</li>
        </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="container-fluid">
          @if(Session::has('messages'))
            <div class="col-xs-12">
              <div class="alert alert-{{ Session::get('type') }} alert-relative alert-padding">
                <button type="button" class="close" data-dismiss="alert"
                          aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                <div>
                  {{ Session::get('messages') }}
                </div>
              </div>
            </div>
          @endif
            <div class="row">
              <div class="col-md-12">
                <div class="card">
                  <div class="card-header">
                  <div class="d-flex">
                    @include('admin.components.search', ['placeholder' => 'Nama'])
                    <div class="ml-auto">
                      <button type="button" class="btn btn-primary btn-add-modal-gallery-category" data-url-add="{{ route('gallery-categories.store') }}">Tambah Data</button>
                    </div>
                  </div>
                  </div>
                  <!-- /.card-header -->
                  <div class="card-body">
                    <div class="table-responsive">
                      <table class="table table-bordered table-striped">
                        <thead class="thead-light">                  
                          <tr>
                            <th style="width: 10px">#</th>
                            <th>Nama</th>
                            <th style="width: 40px">Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          @foreach($datas as $data)
                            <tr>
                              <td>{{ $loop->iteration }}</td>
                              <td>{{ $data->name }}</td>
                              <td class="d-flex">
                                <button type="button" 
                                        class="btn btn-success btn-edit-modal-gallery-category mr-1" 
                                        data-name="{{ $data->name }}" 
                                        data-url-update="{{ route('gallery-categories.update', $data->id) }}"
                                >Edit</button>
                                @if($data->galleries->count() === 0) 
                                  {{ Form::open(['route' => ['gallery-categories.destroy', $data->id], 'onsubmit' => 'message(this, event)', 'data-message' => 'Ingin Menghapus Kategori Galeri ini', 'method' => 'DELETE']) }}
                                    <button class="btn btn-danger">Hapus</button>
                                {{ Form::close() }}
                                @else
                                  <button type="button" class="btn btn-default" onclick="messageDefault('Gagal menghapus', 'Tidak Bisa menghapus karena sudah terpakai di blog', 'error')" style="cursor:not-allowed">Hapus</button>
                                @endif
                              </td>
                            </tr>
                          @endforeach
                        </tbody>
                      </table>
                    </div>
                  </div>
                  <div class="card-footer d-flex justify-content-end">
                    {{ $datas->appends(request()->all())->links() }}
                  </div>
                </div>
                <!-- /.card -->
              </div>
              <!-- /.col -->
            </div>
          </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('js')
  <script>
    $('.btn-add-modal-gallery-category').click(function() {
      $('#modal-gallery-category').modal('show');
      $('#modal-gallery-category').find('#gallery-category-form').attr('action', $(this).data('url-add'));
      $('#modal-gallery-category').find('#name').val('');
      $('#modal-gallery-category').find('.modal-title').html('Tambah Data Kategori Galeri');
      $('#modal-gallery-category').find("input[name='_method']").attr('disabled', true);
    });

    $('.btn-edit-modal-gallery-category').click(function() {
      $('#modal-gallery-category').modal('show');
      $('#modal-gallery-category').find('#gallery-category-form').attr('action', $(this).data('url-update'));
      $('#modal-gallery-category').find('#name').val($(this).data('name'));
      $('#modal-gallery-category').find('.modal-title').html('Edit Data Kategori Galeri');
      $('#modal-gallery-category').find("input[name='_method']").attr('disabled', false);
    });
  </script>
@endsection