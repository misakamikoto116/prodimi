@extends('admin.layouts.main')
 
@section('titlehome','User')  

@section('content') 
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
        <h1>
            User
            <small>List User</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">List User</li>
        </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="container-fluid">
          @if(Session::has('messages'))
            <div class="col-xs-12">
              <div class="alert alert-{{ Session::get('type') }} alert-relative alert-padding">
                <button type="button" class="close" data-dismiss="alert"
                          aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                <div>
                  {{ Session::get('messages') }}
                </div>
              </div>
            </div>
          @endif
            <div class="row">
              <div class="col-md-12">
                <div class="card">
                  <div class="card-header">
                  <div class="d-flex">
                    @include('admin.components.search', ['placeholder' => 'Nama, Email'])
                    <div class="ml-auto">
                       <a href="{{ route('user.create') }}"><button type="button" class="btn btn-primary">Tambah Data</button></a>
                    </div>
                  </div>
                  </div>
                  <!-- /.card-header -->
                  <div class="card-body">
                    <div class="table-responsive">
                      <table class="table table-bordered table-striped">
                        <thead class="thead-light">                  
                          <tr>
                            <th style="width: 10px">#</th>
                            <th>Nama</th>
                            <th>Foto</th>
                            <th>Email</th>
                            <th style="width: 40px">Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          @foreach($datas as $data)
                            <tr>
                              <td>{{ $loop->iteration }}</td>
                              <td>{{ $data->name }}</td>
                              <td>
                                <img src="{{ asset('storage/users/' . $data->img) }}" class="img-thumbnail" style="height: 150px;width: 150px" alt="">
                              </td>
                              <td>{{ $data->email }}</td>
                              <td class="d-flex">
                              @if(auth()->user()->id === 1)
                                <a href="{{ route('user.edit', $data->id ) }}"><button type="button" class="btn btn-success btn-edit-modal-blog mr-1">Edit</button></a>
                                @if($data->blogs->count() === 0) 
                                  {{ Form::open(['route' => ['user.destroy', $data->id], 'onsubmit' => 'message(this, event)', 'data-message' => 'Ingin Menghapus Data user ini', 'method' => 'DELETE']) }}
                                    <button class="btn btn-danger mr-1">Hapus</button>
                                  {{ Form::close() }}
                                @else
                                  <button type="button" class="btn btn-default" onclick="messageDefault('Gagal menghapus', 'Tidak Bisa menghapus karena sudah membuat blog', 'error')" style="cursor:not-allowed">Hapus</button>
                                @endif
                              @else
                              -
                              @endif
                              </td>
                            </tr>
                          @endforeach
                        </tbody>
                      </table>
                    </div>
                  </div>
                  <div class="card-footer d-flex justify-content-end">
                    {{ $datas->appends(request()->all())->links() }}
                  </div>
                </div>
                <!-- /.card -->
              </div>
              <!-- /.col -->
            </div>
          </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection