@extends('admin.layouts.main')
 
@section('titlehome','Dashboard')  

@section('content') 
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
        <h1>
            User
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('user.index') }}"><i class="fa fa-newspaper-o"></i> Data User</a></li>
            <li class="active">{{ isset($data) ? 'Edit' : 'Tambah' }} Data User</li>
        </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          @if(Session::has('messages'))
            <div class="col-xs-12">
              <div class="alert alert-{{ Session::get('type') }} alert-relative alert-padding">
                <button type="button" class="close" data-dismiss="alert"
                          aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                <div>
                  {{ Session::get('messages') }}
                </div>
              </div>
            </div>
          @endif
          <div class="row">
            <div class="col-md-12">
              <div class="card card-primary">
                <div class="card-header">
                  <h3 class="card-title">{{ isset($data) ? 'Edit' : 'Tambah' }} Data User</h3>
                </div>
                @if(isset($data))
                  {{ Form::open(['route' => ['user.update', $data->id], 'method' => 'PUT', 'files' => true]) }} 
                @else
                {{ Form::open(['route' => ['user.store'], 'method' => 'POST', 'files' => true]) }} 
                @endif

                {{ Form::token() }}
                <div class="card-body">
                  <div class="form-group">
                    {{ Form::label('name', 'Name') }}
                    {{ Form::text('name', isset($data) ? old('name', $data->name) : old('name'), ['class' => 'form-control', 'id' => 'name', 'placeholder' => 'Masukkan Nama', 'required' => true]) }}
                    @error('name')
                      <div class="error text-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  @if(!isset($data))
                    <div class="form-group">
                      {{ Form::label('email', 'Email') }}
                      {{ Form::email('email', isset($data) ? old('email', $data->email) : old('email'), ['class' => 'form-control', 'id' => 'email', 'placeholder' => 'Masukkan Email', 'required' => true]) }}
                      @error('email')
                        <div class="error text-danger">{{ $message }}</div>
                      @enderror
                    </div>
                  @endif
                  @if(!isset($data))
                    <div class="form-group">
                      {{ Form::label('password', 'Password') }}
                      {{ Form::password('password', ['class' => 'form-control', 'id' => 'password', 'placeholder' => 'Masukkan Password', 'required' => true, 'min' => 6, 'max' => '20']) }}
                      @error('password')
                        <div class="error text-danger">{{ $message }}</div>
                      @enderror
                    </div>
                  @endif
                  <label for="">Gambar</label>
                  @if(isset($data))
                    <div class="form-group">
                      <img src="{{ asset('storage/users/' . $data->img) }}" class="img-thumbnail" style="height: 300px;" alt="">
                    </div>
                  @endif

                  <label for="">Upload gambar Baru</label>
                  <div class="form-group">
                    {{ Form::file('img', ['class' => 'form-control pb-2', 'accept' => 'image/*', isset($data) ?? 'required' => true]) }}
                    <div class="border">
                      <ul class="pt-2">
                      @if(isset($data))
                        <li>Pilih gambar untuk mengubah gambar</li>
                        <li>Tidak perlu pilih gambar jika tidak ingin mengubah gambar</li>
                      @endif
                      <li>File harus gambar</li>
                    </ul>
                    </div>
                  </div>
                </div>
                <div class="card-footer clearfix">
                  <div class="col-12">
                    {{ Form::submit('Save changes', ['class' => 'btn btn-success float-right']) }}
                  </div>
                </div>
                {{ Form::close() }}
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
            </div>
          </div>
        </section>
        <!-- /.content -->
    </div>
@endsection