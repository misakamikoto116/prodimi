@extends('admin.layouts.main')
 
@section('titlehome','Jadwal Perkuliahan')  

@section('content') 
    @include('admin.archive.jadwal_perkuliahan.templates.modal')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
        <h1>
            Jadwal Perkuliahan
            <small>List Jadwal Perkuliahan</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">List Jadwal Perkuliahan</li>
        </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="container-fluid">
          @if(Session::has('messages'))
            <div class="col-xs-12">
              <div class="alert alert-{{ Session::get('type') }} alert-relative alert-padding">
                <button type="button" class="close" data-dismiss="alert"
                          aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                <div>
                  {{ Session::get('messages') }}
                </div>
              </div>
            </div>
          @endif
            <div class="row">
              <div class="col-md-12">
                <div class="card">
                  <div class="card-header">
                  <div class="d-flex">
                    @include('admin.components.search', ['placeholder' => 'Tahun'])
                    <div class="ml-auto">
                      <button type="button" class="btn btn-primary btn-add-modal-archive" data-url-add="{{ route('jadwal-perkuliahan.store') }}">Tambah Data</button>
                    </div>
                  </div>
                  </div>
                  <!-- /.card-header -->
                  <div class="card-body">
                    <div class="table-responsive">
                      <table class="table table-bordered table-striped">
                        <thead class="thead-light">                  
                          <tr>
                            <th style="width: 10px">#</th>
                            <th>Tahun</th>
                            <th>File</th>
                            <th style="width: 40px">Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          @foreach($datas as $data)
                            <tr>
                              <td>{{ $loop->iteration }}</td>
                              <td>{{ $data->tahun }}</td>
                              <td><a href="{{ asset('storage/jadwal-perkuliahan/' . $data->file) }}" target="_blank"><button class="btn btn-info">Download</button></a></td>
                              <td class="d-flex">
                                <button type="button" 
                                        class="btn btn-success btn-edit-modal-archive mr-1" 
                                        data-tahun="{{ $data->tahun }}" 
                                        data-url-update="{{ route('jadwal-perkuliahan.update', $data->id) }}"
                                >Edit</button>
                                {{ Form::open(['route' => ['jadwal-perkuliahan.destroy', $data->id], 'onsubmit' => 'message(this, event)', 'data-message' => 'Ingin Menghapus Archive ini', 'method' => 'DELETE']) }}
                                    <button class="btn btn-danger">Hapus</button>
                                {{ Form::close() }}
                              </td>
                            </tr>
                          @endforeach
                        </tbody>
                      </table>
                    </div>
                  </div>
                  <div class="card-footer d-flex justify-content-end">
                    {{ $datas->appends(request()->all())->links() }}
                  </div>
                </div>
                <!-- /.card -->
              </div>
              <!-- /.col -->
            </div>
          </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('js')
  <script>
    $('.btn-add-modal-archive').click(function() {
      $('#modal-archive').modal('show');
      $('#modal-archive').find('#archive-form').attr('action', $(this).data('url-add'));
      $('#modal-archive').find('#tahun').val('');
      $('#modal-archive').find('.form-edit').hide();
      $('#modal-archive').find('.modal-title').html('Tambah Data Jadwal Perkuliahan');
      $( "input[name='_method']").attr('disabled', true);
    });

    $('.btn-edit-modal-archive').click(function() {
      $('#modal-archive').modal('show');
      $('#modal-archive').find('#archive-form').attr('action', $(this).data('url-update'));
      $('#modal-archive').find('#tahun').val($(this).data('tahun'));
      $('#modal-archive').find('.form-edit').show();
      $('#modal-archive').find('.modal-title').html('Edit Data Jadwal Perkuliahan');
      $( "input[name='_method']").attr('disabled', false);
    });
  </script>
@endsection