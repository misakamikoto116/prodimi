@extends('admin.layouts.main')
 
@section('titlehome','Alumni')  

@section('content') 
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
        <h1>
            Alumni
            <small>List Alumni</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">List Alumni</li>
        </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="container-fluid">
            @if(Session::has('messages'))
              <div class="col-xs-12">
                <div class="alert alert-{{ Session::get('type') }} alert-relative alert-padding">
                  <button type="button" class="close" data-dismiss="alert"
                            aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                  <div>
                    {{ Session::get('messages') }}
                  </div>
                </div>
              </div>
            @endif
            <div class="row">
              <div class="col-md-12">
                <div class="card">
                  <div class="card-header">
                    <div class="d-flex">
                      @include('admin.components.search', ['placeholder' => 'Nama, Angkatan, Job, Telepon'])
                      <div class="pt-2">
                        <a href="{{ route('alumni.create') }}"><button type="button" class="btn btn-primary mr-1">Tambah Data</button></a>
                      </div>
                      <div class="pt-2">
                        <a href="{{ route('alumni.print') }}" title="print out" target="_blank"><button type="button" class="btn btn-success"><i class="fa fa-print"></i></button></a>
                      </div>
                    </div>
                    <div class="mt-2 mb-2">
                    @if(Request::has('verif') || Request::has('q') || Request::has('homepage'))
                        <a class="label label-info" href="{{ url('admin/alumni') }}">Semua</a>
                        <div class="clearfix"></div>
                        <br>
                    @endif
                    <a class="label label-primary" href="{{ url('admin/alumni?verif=1') }}">Terverifikasi</a>
                    <a class="label label-danger" href="{{ url('admin/alumni?verif=0') }}">Belum Diverifikasi</a>
                    <a class="label label-info" href="{{ url('admin/alumni?homepage=1') }}">Ditampilkan Dari Homepage</a>
                    <a class="label label-warning" href="{{ url('admin/alumni?homepage=0') }}">Disembunyikan Dari Homepage</a>
                  </div>
                  </div>
                  <!-- /.card-header -->
                  <div class="card-body">
                    <div class="table-responsive">
                      <table class="table table-bordered table-striped">
                        <thead class="thead-light">                  
                          <tr>
                            <th style="width: 10px">#</th>
                            <th>Nama</th>
                            <th>Foto</th>
                            <th>Angkatan</th>
                            <th>Pekerjaan</th>
                            <th>No Telepon</th>
                            <th>verif</th>
                            <th>homepage</th>
                            <th style="width: 40px">Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          @foreach($datas as $data)
                            <tr>
                              <td>{{ $loop->iteration }}</td>
                              <td>{{ $data->nama }}</td>
                              <td>
                                <img src="{{ asset('storage/alumnis/' . $data->img) }}" class="img-thumbnail" style="height: 150px;width: 150px" alt="">
                              </td>
                              <td>{{ $data->angkatan }}</td>
                              <td>{{ $data->job->name }}</td>
                              <td>{{ $data->telp }}</td>
                              <th class="text-{{ $data->verif ? 'success' : 'danger' }}">{{ $data->verif ? 'Terverifikasi' : 'Belum Diverifikasi' }}</th>
                              <th>
                                {{ Form::open(['route' => ['alumni.homepage', $data->id], 'method' => 'POST']) }}
                                  <button class="btn btn-{{ $data->homepage ? 'success' : 'danger' }}" title="{{ $data->homepage ? 'Sembunyikan' : 'Tampilkan' }}">{{ $data->homepage ? 'Ditampilkan' : 'Disembunyikan' }}</button>
                                {{ Form::close() }}
                              </th>
                              <td class="d-flex">
                              <a href="{{ route('alumni.edit', $data->id ) }}"><button type="button" class="btn btn-success btn-edit-modal-blog mr-1">Edit</button></a>
                                {{ Form::open(['route' => ['alumni.destroy', $data->id], 'onsubmit' => 'message(this, event)', 'data-message' => 'Ingin Menghapus Data Alumni ini', 'method' => 'DELETE']) }}
                                  <button class="btn btn-danger mr-1">Hapus</button>
                                {{ Form::close() }}
                                @if(!$data->verif)
                                  {{ Form::open(['route' => ['alumni.verif', $data->id], 'onsubmit' => 'message(this, event)', 'data-message' => 'Ingin Memverifikasi Alumni ini', 'method' => 'POST']) }}
                                    <button class="btn btn-info" title="Verifikasi">Verifikasi</button>
                                  {{ Form::close() }}
                                @endif
                              </td>
                            </tr>
                          @endforeach
                        </tbody>
                      </table>
                    </div>
                  </div>
                  <div class="card-footer d-flex justify-content-end">
                    {{ $datas->appends(request()->all())->links() }}
                  </div>
                </div>
                <!-- /.card -->
              </div>
              <!-- /.col -->
            </div>
          </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection