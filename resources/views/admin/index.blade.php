@extends('admin.layouts.main')
 
@section('titlehome','Dashboard')  

@section('content') 
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
        <h1>
            Dashboard
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
        </section>

        <!-- Main content -->
        <section class="content">
        <div class="container-fluid">
            <div class="row">
              <div class="col-md-12">
                <div class="card">
                  <div class="card-header">
                    <h3 class="card-title">Welcome!</h3>
                  </div>
                  <!-- /.card-header -->
                  <div class="card-body">
                      Selamat Datang Di Aplikasi Admin Prodi
                  </div>
                </div>
                <!-- /.card -->
              </div>
              <!-- /.col -->
            </div>
            <br>
            <div class="box box-primary">
              <div class="box-header with-border">
                <i class="fa fa-bar-chart-o"></i>
    
                <h3 class="box-title">Statistik Pekerjaan Alumni</h3>
    
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
              </div>
              <div class="box-body">
                <div id="bar-chart" style="height: 300px;"></div>
              </div>
              <!-- /.box-body-->
            </div>
          </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('js')
  <script>
    /*
      * BAR CHART
      * ---------
      */

    var bar_data = {
      data : [['Bidang IT', {{$BI}}], ['PNS', {{$PNS}}], ['Wirausahawan', {{$W}}], ['Olahraga', {{$O}}], ['Dan Lain Lain', {{$DLL}}]],
      color: '#3c8dbc'
    }
    $.plot('#bar-chart', [bar_data], {
      grid  : {
        borderWidth: 1,
        borderColor: '#f3f3f3',
        tickColor  : '#f3f3f3'
      },
      series: {
        bars: {
          show    : true,
          barWidth: 0.5,
          align   : 'center'
        }
      },
      xaxis : {
        mode      : 'categories',
        tickLength: 0
      }
    })
    /* END BAR CHART */
  </script>
@endsection