<div class="pt-2 mr-auto" style="flex: 0.4 0 auto;">
  <form method="get" role="form" action="{{ url()->current() }}">
      <div class="input-group">
        <input type="text" name="q" class="form-control" placeholder="Cari {{ $placeholder }}" value="{{ Request::get('q') }}">
        <div class="input-group-append">
          <button class="btn btn-secondary">
            <i class="fa fa-search"></i>
          </button>
        </div>
      </div>
  </form>
</div>