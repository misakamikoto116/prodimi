@extends('admin.layouts.main')
 
@section('titlehome','Dashboard')  

@section('content') 
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
        <h1>
            Update Profile
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Profile</li>
        </ol>
        </section>

        
        <!-- Main content -->
        <section class="content">
          @if(Session::has('messages'))
            <div class="col-xs-12">
              <div class="alert alert-{{ Session::get('type') }} alert-relative alert-padding">
                <button type="button" class="close" data-dismiss="alert"
                          aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                <div>
                  {{ Session::get('messages') }}
                </div>
              </div>
            </div>
          @endif
          <div class="row">
            <div class="col-md-12">
              <div class="card card-primary">
                <div class="card-header">
                  <h3 class="card-title">Ubah Profile</h3>
                </div>
                {{ Form::open(['route' => ['admin.post.profile'], 'method' => 'POST', 'files' => true]) }} 

                {{ Form::token() }}
                <div class="card-body">
                  <div class="form-group">
                    {{ Form::label('name', 'Name') }}
                    {{ Form::text('name', old('name', auth()->user()->name), ['class' => 'form-control', 'id' => 'name', 'placeholder' => 'Masukkan Nama', 'required' => true]) }}
                    @error('name')
                      <div class="error text-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  <label for="">Gambar</label>
                  <div class="form-group">
                    <img src="{{ asset('storage/users/' . auth()->user()->img) }}" class="img-thumbnail" style="height: 300px;" alt="">
                  </div>

                  <label for="">Upload gambar Baru</label>
                  <div class="form-group">
                    {{ Form::file('img', ['class' => 'form-control pb-2', 'accept' => 'image/*']) }}
                    <div class="border">
                      <ul class="pt-2">
                      <li>Pilih gambar untuk mengubah gambar</li>
                      <li>Tidak perlu pilih gambar jika tidak ingin mengubah gambar</li>
                      <li>File harus gambar</li>
                    </ul>
                    </div>
                  </div>
                  <div class="form-group">
                    {{ Form::label('password', 'Password Anda (Saat ini)') }}
                    {{ Form::password('password', ['class' => 'form-control', 'id' => 'password', 'placeholder' => 'Masukkan Password Anda Yang Sekarang', 'required' => true, 'min' => 6, 'max' => '20']) }}
                    @error('password')
                      <div class="error text-danger">{{ $message }}</div>
                    @enderror
                  </div>
                </div>
                <div class="card-footer clearfix">
                  <div class="col-12">
                    {{ Form::submit('Save changes', ['class' => 'btn btn-success float-right']) }}
                  </div>
                </div>
                {{ Form::close() }}
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
            </div>
          </div>
        </section>
        <!-- /.content -->
    </div>
@endsection