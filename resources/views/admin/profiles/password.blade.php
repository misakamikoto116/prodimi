@extends('admin.layouts.main')
 
@section('titlehome','Dashboard')  

@section('content') 
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
        <h1>
            Blog
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Ubah Password</li>
        </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          @if(Session::has('messages'))
            <div class="col-xs-12">
              <div class="alert alert-{{ Session::get('type') }} alert-relative alert-padding">
                <button type="button" class="close" data-dismiss="alert"
                          aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                <div>
                  {{ Session::get('messages') }}
                </div>
              </div>
            </div>
          @endif
          <div class="row">
            <div class="col-md-12">
              <div class="card card-primary">
                <div class="card-header">
                  <h3 class="card-title">Ubah Password</h3>
                </div>
                {{ Form::open(['route' => 'admin.post.password', 'method' => 'POST']) }} 

                {{ Form::token() }}
                <div class="card-body">
                  <div class="form-group">
                    {{ Form::label('new_password', 'Password Baru') }}
                    {{ Form::password('new_password', ['class' => 'form-control', 'id' => 'new_password', 'placeholder' => 'Masukkan Password', 'required' => true, 'min' => 6, 'max' => '20']) }}
                    @error('password')
                      <div class="error text-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    {{ Form::label('password', 'Password') }}
                    {{ Form::password('password', ['class' => 'form-control', 'id' => 'password', 'placeholder' => 'Masukkan Password', 'required' => true, 'min' => 6, 'max' => '20']) }}
                    @error('password')
                      <div class="error text-danger">{{ $message }}</div>
                    @enderror
                  </div>
                </div>
                <div class="card-footer clearfix">
                  <div class="col-12">
                    {{ Form::submit('Save changes', ['class' => 'btn btn-success float-right']) }}
                  </div>
                </div>
                {{ Form::close() }}
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
            </div>
          </div>
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('js')
  <script>
    @if(isset($data))
      $("#selected").val({{  $data->category_id }}).trigger('change');
    @endif
  </script>

@endsection