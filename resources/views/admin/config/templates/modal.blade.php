<div class="modal fade" id="modal-edit-config" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Edit Config</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      {{ Form::open(['route' => 'config.index', 'method' => 'PUT', 'id' => 'config-form']) }} 
        <div class="modal-body">
          <div class="form-group">
            {{ Form::label('name', 'Name') }}
            {{ Form::text('name', '', ['class' => 'form-control', 'id' => 'name', 'disabled' => true]) }}
          </div>
          <div class="form-group">
            {{ Form::label('value', 'Value') }}
            {{ Form::text('value', '', ['class' => 'form-control', 'id' => 'value', 'placeholder' => 'Masukkan Value' , 'required' => true]) }}
          </div>
        </div>
        <div class="modal-footer">
          {{ Form::submit('Save changes', ['class' => 'btn btn-primary']) }}
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      {{ Form::close() }}
    </div>
  </div>
</div>