@extends('admin.layouts.main')
 
@section('titlehome','Config')  

@section('content') 
    @include('admin.config.templates.modal')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
        <h1>
            Config
            <small>List Config</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">List Config</li>
        </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="container-fluid">
          @if(Session::has('messages'))
            <div class="col-xs-12">
              <div class="alert alert-{{ Session::get('type') }} alert-relative alert-padding">
                <button type="button" class="close" data-dismiss="alert"
                          aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                <div>
                  {{ Session::get('messages') }}
                </div>
              </div>
            </div>
          @endif
            <div class="row">
              <div class="col-md-12">
                <div class="card">
                  <div class="card-header">
                    <h3 class="card-title">Data Config</h3>
                  </div>
                  <!-- /.card-header -->
                  <div class="card-body">
                    <table class="table table-bordered table-striped">
                      <thead class="thead-light">                  
                        <tr>
                          <th style="width: 10px">#</th>
                          <th width="15%">Name</th>
                          <th>Value</th>
                          <th style="width: 40px">Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($datas as $data)
                          <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $data->name }}</td>
                            <td>{{ $data->value }}</td>
                            <td>
                              <button type="button" 
                                      class="btn btn-success btn-edit-modal-config" 
                                      data-name="{{ $data->name }}" 
                                      data-value="{{ $data->value }}"
                                      data-url-update="{{ route('config.update', $data->id) }}"
                              >Edit</button>
                            </td>
                          </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
                <!-- /.card -->
              </div>
              <!-- /.col -->
            </div>
          </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('js')
  <script>
    $('.btn-edit-modal-config').click(function() {
      $('#modal-edit-config').modal('show');
      $('#modal-edit-config').find('#config-form').attr('action', $(this).data('url-update'));
      $('#modal-edit-config').find('#name').val($(this).data('name'));
      $('#modal-edit-config').find('#value').val($(this).data('value'));
    });
  </script>
@endsection