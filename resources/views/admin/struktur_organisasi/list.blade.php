@extends('admin.layouts.main')
 
@section('titlehome','Struktur Organisasi')  

@section('content') 
    @include('admin.struktur_organisasi.templates.modal')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
        <h1>
            Struktur Organisasi
            <small>List Struktur Organisasi</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">List Struktur Organisasi</li>
        </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="container-fluid">
          @if(Session::has('messages'))
            <div class="col-xs-12">
              <div class="alert alert-{{ Session::get('type') }} alert-relative alert-padding">
                <button type="button" class="close" data-dismiss="alert"
                          aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                <div>
                  {{ Session::get('messages') }}
                </div>
              </div>
            </div>
          @endif
            <div class="row">
              <div class="col-md-12">
                <div class="card">
                  <div class="card-header">
                    <div class="d-flex">
                      @include('admin.components.search', ['placeholder' => 'Nama, Jabatan, NIM'])
                    </div>
                  </div>
                  <!-- /.card-header -->
                  <div class="card-body">
                    <div class="table-responsive">
                      <table class="table table-bordered table-striped">
                        <thead class="thead-light">                  
                          <tr>
                            <th style="width: 10px">#</th>
                            <th>Nama</th>
                            <th>Jabatan</th>
                            <th>NIM</th>
                            <th style="width: 40px">Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          @foreach($datas as $data)
                            <tr>
                              <td>{{ $loop->iteration }}</td>
                              <td>{{ $data->name }}</td>
                              <td>{{ $data->jabatan }}</td>
                              <td>{{ $data->nim }}</td>
                              <td class="d-flex">
                                <button type="button" 
                                        class="btn btn-success btn-edit-modal-struktur-organisasi mr-1" 
                                        data-name="{{ $data->name }}" 
                                        data-jabatan="{{ $data->jabatan }}" 
                                        data-nim="{{ $data->nim }}" 
                                        data-url-update="{{ route('struktur-organisasi.update', $data->id) }}"
                                >Edit</button>
                              </td>
                            </tr>
                          @endforeach
                        </tbody>
                      </table>
                    </div>
                  </div>
                  <div class="card-footer d-flex justify-content-end">
                    {{ $datas->appends(request()->all())->links() }}
                  </div>
                </div>
                <!-- /.card -->
              </div>
              <!-- /.col -->
            </div>
          </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('js')
  <script>
    $('.btn-edit-modal-struktur-organisasi').click(function() {
      $('#modal-struktur-organisasi').modal('show');
      $('#modal-struktur-organisasi').find('#struktur-organisasi-form').attr('action', $(this).data('url-update'));
      $('#modal-struktur-organisasi').find('#name').val($(this).data('name'));
      $('#modal-struktur-organisasi').find('#jabatan').val($(this).data('jabatan'));
      $('#modal-struktur-organisasi').find('#nim').val($(this).data('nim'));
      $('#modal-struktur-organisasi').find('.modal-title').html('Edit Data Struktur Organisasi');
      $( "input[name='_method']").attr('disabled', false);
    });
  </script>
@endsection