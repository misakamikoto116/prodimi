<div class="modal fade" id="modal-struktur-organisasi" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      {{ Form::open(['route' => 'struktur-organisasi.store', 'method' => 'PUT', 'id' => 'struktur-organisasi-form']) }} 
        <div class="modal-body">
          <div class="form-group">
            {{ Form::label('name', 'Name') }}
            {{ Form::text('name', '', ['class' => 'form-control', 'id' => 'name', 'placeholder' => 'Masukkan Nama', 'required' => true]) }}
          </div>
          <div class="form-group">
            {{ Form::label('jabatan', 'Jabatan') }}
            {{ Form::text('jabatan', '', ['class' => 'form-control', 'id' => 'jabatan', 'placeholder' => 'Masukkan Jabatan', 'disabled' => true]) }}
          </div>
          <div class="form-group">
            {{ Form::label('NIM', 'NIM') }}
            {{ Form::number('NIM', '', ['class' => 'form-control', 'id' => 'nim', 'placeholder' => 'Masukkan NIM', 'required' => true]) }}
          </div>
        </div>
        <div class="modal-footer">
          {{ Form::submit('Save changes', ['class' => 'btn btn-primary']) }}
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      {{ Form::close() }}
    </div>
  </div>
</div>