@extends('admin.layouts.main')
 
@section('titlehome','Sambutan')  

@section('content') 
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
        <h1>
            Sambutan
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Data Sambutan</li>
        </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          @if(Session::has('messages'))
            <div class="col-xs-12">
              <div class="alert alert-{{ Session::get('type') }} alert-relative alert-padding">
                <button type="button" class="close" data-dismiss="alert"
                          aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                <div>
                  {{ Session::get('messages') }}
                </div>
              </div>
            </div>
          @endif
          <div class="row">
            <div class="col-md-12">
              <div class="card card-primary">
                <div class="card-header">
                  <h3 class="card-title">Data Sambutan</h3>
                </div>
                {{ Form::open(['route' => ['welcome.update', $data->id], 'method' => 'PUT', 'files' => true]) }} 
                {{ Form::token() }}
                <div class="card-body">
                  <div class="form-group">
                    {{ Form::label('nama', 'Nama') }}
                    {{ Form::text('nama', old('name', $data->name), ['class' => 'form-control', 'id' => 'nama', 'placeholder' => 'Masukkan Nama' , 'required' => true]) }}
                  </div>
                  <div class="form-group">
                    {{ Form::label('quote', 'Quote') }}
                    <textarea class="textarea" name="quote" placeholder="Message" style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{ old('quote', $data->quote) }}</textarea>
                  </div>
                  <label for="">Gambar</label>
                  <div class="form-group">
                    @if(!empty($data->img))
                      <img src="{{ asset('storage/sambutan/' . $data->img) }}" class="img-thumbnail" style="height: 300px;" alt="">
                    @else
                      <img src="{{ asset('front/img/profil/kaprodi.jpg') }}" class="img-thumbnail" style="height: 300px;" alt="">
                    @endif
                  </div>

                  <label for="">Upload gambar Baru</label>
                  <div class="form-group">
                    {{ Form::file('img', ['class' => 'form-control pb-2', 'accept' => 'image/*']) }}
                    <div class="border">
                      <ul class="pt-2">
                      <li>Pilih gambar untuk mengubah gambar</li>
                      <li>Tidak perlu pilih gambar jika tidak ingin mengubah gambar</li>
                      <li>File harus gambar</li>
                    </ul>
                    </div>
                  </div>
                </div>
                <div class="card-footer clearfix">
                  <div class="col-12">
                    {{ Form::submit('Save changes', ['class' => 'btn btn-success float-right']) }}
                  </div>
                </div>
                {{ Form::close() }}
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
            </div>
          </div>
        </section>
        <!-- /.content -->
    </div>
@endsection