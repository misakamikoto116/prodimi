<?php

use Illuminate\Database\Seeder;
use App\Models\StrukturOrganisasi;

class StrukturOrganisasiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['name' => 'Ketua Hima', 'jabatan' => 'Ketua Hima', 'nim' => '000000000001'],
            ['name' => 'Wakil Ketua Hima', 'jabatan' => 'Wakil Ketua Hima', 'nim' => '000000000002'],
            ['name' => 'Bendahara', 'jabatan' => 'Bendahara', 'nim' => '000000000003'],
            ['name' => 'Sekretaris', 'jabatan' => 'Sekretaris', 'nim' => '000000000004'],
            ['name' => 'IPTEK', 'jabatan' => 'IPTEK', 'nim' => '000000000005'],
            ['name' => 'Infokom', 'jabatan' => 'Infokom', 'nim' => '000000000006'],
            ['name' => 'Humas', 'jabatan' => 'Humas', 'nim' => '000000000007'],
            ['name' => 'Minat Bakat', 'jabatan' => 'Minat Bakat', 'nim' => '000000000008'],
        ];

        StrukturOrganisasi::insert($data);
    }
}
