<?php

use Illuminate\Database\Seeder;
use App\Models\Tahunakademik;

class TahunAkademikSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = Tahunakademik::create([
            'tahun' => '2020'
        ]);
        
        $this->command->info('Tahun akademik berhasil dibuat.');
    }
}
