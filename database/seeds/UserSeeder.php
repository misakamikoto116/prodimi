<?php

use Illuminate\Database\Seeder;
use App\User;
use Carbon\Carbon;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = User::create([
            'name'          => 'Administrator', // Nama Lengkap
            'email'         => 'admin@gmail.com', // Email
            'password'      => bcrypt('123123'), // password
        ]);

        $this->command->info('User Admin berhasil dibuat.'); // Terminal info
    }
}
