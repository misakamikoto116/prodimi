<?php

use Illuminate\Database\Seeder;
use App\Models\Configs;

class ConfigSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['name' => 'Visi', 'value' => 'Contoh Visi'],
            ['name' => 'Misi', 'value' => 'Contoh Misi'],
            ['name' => 'Tujuan', 'value' => 'Contoh tujuan'],
            ['name' => 'Tujuan Pendidikan', 'value' => 'Contoh Tujuan Pendidikan'],
            ['name' => 'Profil Pendidikan', 'value' => 'Contoh Profil Pendidikan'],
            ['name' => 'Kurikulum', 'value' => 'Contoh kurikulum'],
            ['name' => 'Mahasiswa dan alumni', 'value' => 'Contoh Mahasiswa dan Alumni'],
            ['name' => 'Kerja Sama', 'value' => 'Contoh Kerja Sama'],
        ];

        Configs::insert($data);
    }
}
