<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);
        $this->call(ConfigSeeder::class);
        $this->call(TahunAkademikSeeder::class);
        $this->call(SambutanSeeder::class);
        $this->call(GalleryCategorySeeder::class);
        $this->call(StrukturOrganisasiSeeder::class);
        $this->call(PekerjaanAlumniSeeder::class);
    }
}
