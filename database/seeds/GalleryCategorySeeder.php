<?php

use Illuminate\Database\Seeder;
use App\Models\GalleryCategories;

class GalleryCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = GalleryCategories::create([
            'name' => 'Kemahasiswaan'
        ]);
        
        $this->command->info('Kategori Galeri berhasil dibuat.');
    }
}
