<?php

use App\Models\PekerjaanAlumni;
use Illuminate\Database\Seeder;

class PekerjaanAlumniSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['name' => 'Bidang IT'],
            ['name' => 'PNS'],
            ['name' => 'Wirausahawan'],
            ['name' => 'Olahraga'],
            ['name' => 'Dan Lain Lain'],
        ];

        PekerjaanAlumni::insert($data);
    }
}
