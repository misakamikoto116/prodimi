<?php

use Illuminate\Database\Seeder;
use App\Models\Sambutan;

class SambutanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = Sambutan::create([
            'name' => 'Jon Doe',
            'quote' => 'Testing Quote',
            'img'   => null
        ]);
        
        $this->command->info('Sambutan berhasil dibuat.');
    }
}
