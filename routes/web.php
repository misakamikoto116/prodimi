<?php

use Illuminate\Support\Facades\Auth;

Route::get('/', 'HomePageController@index')->name('index');
Route::get('/profil', 'HomePageController@profile')->name('profile');
Route::get('/berita', 'HomePageController@berita')->name('berita');

Route::get('/berita/{id}', 'HomePageController@beritaDetail')->name('berita.detail');

Route::get('/galleries', 'HomePageController@galleries')->name('galleries');
Route::get('/archives', 'HomePageController@archives')->name('archives');
Route::get('/dosen', 'HomePageController@dosen')->name('dosen');
Route::get('/struktur-organisasi', 'HomePageController@StrukturOrganisasi')->name('struktur-organisasi');
Route::get('/kegiatan', 'HomePageController@kegiatan')->name('kegiatan');
Route::get('/alumni', 'HomePageController@alumni')->name('alumni');
Route::post('/alumni-landing', 'AlumniController@store')->name('landing.alumni.store');

Route::get('/logout', function () {
  Auth::logout();

  return redirect('/login');
})->name('logout');

Auth::routes();

Route::resource('alumni', 'AlumniController')->only('store');


Route::group(['middleware' => 'auth'], function () {
  Route::prefix('admin')->group(function () {
    Route::get('dashboard', 'HomePageController@dashboard')->name('admin.dashboard');
    Route::get('password', 'ProfileController@password')->name('admin.get.password');
    Route::post('password', 'ProfileController@passwordPost')->name('admin.post.password');
    Route::get('profile', 'ProfileController@profile')->name('admin.get.profile');
    Route::post('profile', 'ProfileController@profilePost')->name('admin.post.profile');
    Route::resource('alumni', 'AlumniController')->except('store', 'show');
    Route::resource('user', 'UserController');
    Route::post('alumni/homepage/{id}', 'AlumniController@homepageManage')->name('alumni.homepage');
    Route::post('alumni/verif/{id}', 'AlumniController@verification')->name('alumni.verif');
    Route::get('alumni/print', 'AlumniController@print')->name('alumni.print');
    Route::resource('dosen', 'DosenController');
    Route::resource('struktur-organisasi', 'StrukturOrganisasiController')->except('create', 'show', 'edit');
    Route::resource('config', 'ConfigController')->only('index', 'update');
    Route::resource('welcome', 'SambutanController')->only('index', 'update');
    Route::resource('blog-categories', 'CategoryBlogController')->except('create', 'show', 'edit');
    Route::resource('gallery-categories', 'CategoryGalleryController')->except('create', 'show', 'edit');
    Route::resource('blog', 'BlogController');
    Route::post('blog/homepage/{id}', 'BlogController@homepageManage')->name('blog.homepage');
    Route::resource('gallery', 'GalleryController');
    Route::resource('kurikulum', 'ArchiveController')->except('create', 'show', 'edit');
    Route::resource('kalender-akademik', 'ArchiveController')->except('create', 'show', 'edit');
    Route::resource('jadwal-perkuliahan', 'ArchiveController')->except('create', 'show', 'edit');
    Route::resource('berkas-kemahasiswaan', 'BerkasKemahasiswaanController')->except('create', 'show', 'edit');
    Route::resource('tahun-akademik', 'TahunAkademikController')->only('index', 'update');
  });
});